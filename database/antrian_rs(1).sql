-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2020 at 09:55 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `antrian_rs`
--

-- --------------------------------------------------------

--
-- Table structure for table `bpjs`
--

CREATE TABLE `bpjs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bpjs_number` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(25) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bpjs`
--

INSERT INTO `bpjs` (`id`, `user_id`, `bpjs_number`, `name`, `status`, `date_modified`) VALUES
(1, 8, '11254862', 'M. Apri', 'deleted', '2020-06-10 09:34:12'),
(2, 8, '02158591678', 'Mochammad Apri AE', 'active', '2020-06-10 09:35:31'),
(3, 8, '78116519', 'Apri', 'active', '2020-06-10 09:30:17'),
(4, 9, '123456789', 'Rusi', 'active', '2020-06-10 16:26:19'),
(5, 9, '12345675165', 'Rusi A', 'active', '2020-06-10 16:26:19');

-- --------------------------------------------------------

--
-- Table structure for table `day_off`
--

CREATE TABLE `day_off` (
  `id` int(11) NOT NULL,
  `poli_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `day_off_date` date NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `day_off`
--

INSERT INTO `day_off` (`id`, `poli_id`, `doctor_id`, `day_off_date`, `note`) VALUES
(1, 8, 8, '2020-06-11', 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(4) NOT NULL,
  `poli_id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  `status` varchar(12) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `poli_id`, `name`, `phone_number`, `status`, `date_modified`) VALUES
(1, 1, 'Line 01, Untuk pelayanan Informasi ajah', '', 'deleted', '2020-06-06 16:36:01'),
(2, 2, 'Line 02, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:08'),
(3, 3, 'Line 03, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:12'),
(4, 4, 'Line 04, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:14'),
(5, 5, 'Line 05, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:17'),
(8, 8, 'Dr. Subagyo', '02151', 'active', '2020-06-06 16:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

CREATE TABLE `poli` (
  `id` int(11) NOT NULL,
  `prefix_code` varchar(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `close_time` time NOT NULL,
  `max_visitor` int(4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`id`, `prefix_code`, `name`, `close_time`, `max_visitor`, `status`, `date_modified`) VALUES
(1, 'A', 'Pembayaran', '00:00:06', 50, 'deleted', '2020-06-06 16:35:37'),
(2, 'B', 'Customer Service', '00:00:50', 50, 'deleted', '2020-06-06 16:35:31'),
(3, 'C', 'Berkas', '00:00:50', 50, 'deleted', '2020-06-06 16:35:41'),
(7, 'C', 'Berkas', '00:00:50', 50, 'deleted', '2020-05-08 15:38:30'),
(8, 'PD', 'Penyakit Dalam', '13:00:00', 120, 'active', '2020-06-06 16:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `queue_list`
--

CREATE TABLE `queue_list` (
  `id` int(11) NOT NULL,
  `poli_id` int(4) NOT NULL,
  `doctor_id` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bpjs_id` int(11) NOT NULL,
  `queue_code` varchar(8) NOT NULL,
  `queue_number` int(11) NOT NULL,
  `visit_date` date NOT NULL,
  `register_date` datetime NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'new',
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `queue_list`
--

INSERT INTO `queue_list` (`id`, `poli_id`, `doctor_id`, `user_id`, `bpjs_id`, `queue_code`, `queue_number`, `visit_date`, `register_date`, `status`, `note`) VALUES
(4, 8, 8, 8, 2, 'PD001', 1, '2020-06-11', '2020-06-11 15:16:54', 'finish', ''),
(5, 8, 8, 8, 3, 'PD002', 2, '2020-06-11', '2020-06-11 15:17:22', 'finish', ''),
(6, 8, 8, 8, 2, 'PD001', 1, '2020-06-12', '2020-06-12 05:25:05', 'finish', ''),
(7, 8, 8, 8, 3, 'PD002', 2, '2020-06-12', '2020-06-12 05:28:33', 'finish', ''),
(8, 8, 8, 8, 4, 'PD003', 3, '2020-06-12', '2020-06-12 05:28:33', 'finish', ''),
(9, 8, 8, 8, 5, 'PD004', 4, '2020-06-12', '2020-06-12 05:28:33', 'finish', '');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `level` varchar(25) NOT NULL,
  `last_login` datetime NOT NULL,
  `name` varchar(50) NOT NULL,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `name`, `obsolete`, `email`, `phone_number`) VALUES
(1, 'sa', 'be03ccaa6c0cbe1143cd6b8df4fa0a3726e34d3188da0e1668f05f8bd9d175669e6bc82284d4c2d25ec470f377eb5ecfe7926353700dbcec906621674c06bf7cfSqjntJ6tni82OFjrKd18N5wF5xE9Qx2lEigA718pao=', 'sa', '2020-06-12 05:24:47', 'System Administrator', 0, '', '0'),
(2, 'user', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'user', '2020-04-19 12:21:14', 'User', 0, '', '0'),
(3, 'op01', '73826ed17dfaaf31d611250455d840c9aa1113d70c1338f588d63a2d78d0eb91a684ccbcf046eae816c85ef473c5a9b797cc1ad128e52a3d31953540e6565739PUzFuiOPhO5GIlzVP8RylGPVhJIieom5xy6zZ5qa8pc=', 'operator', '2020-06-12 08:46:18', 'Jhoni', 0, '', '0'),
(4, 'op02', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'operator', '2020-05-07 12:15:21', 'Rina', 0, '', '0'),
(5, 'op03', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'operator', '2020-04-04 04:28:59', 'Sabeni', 0, '', '0'),
(8, 'aprigeuza@gmail.com', '6a44c2bc978f9bd432711b923d96ad4cf67292821ab0e977c1e0f72750d810b2d10cf4a062e6fa586cd15f293c561ec2560230425103fd1157a57159d436fcea5CkK+SrWLcIlXHsG+sG36VziO21aNvlUTI7BOysurJk=', 'user', '2020-06-12 05:24:02', 'Apri', 0, 'aprigeuza@gmail.com', '0'),
(9, 'rusiprisie@gmail.com', '0c242282484f71a2bf9a8ed47674b5eedaf42935e4aab691a9686bec15dec840657cfce05902a421a0243bc7b6202dc393b14a673c83f3e05d52856901dd28a9AliZNvIJsAMzA0pV/zFwZN2YfAPupZpe/gLj8jLNvfo=', 'user', '2020-06-10 16:23:37', 'Rusi', 0, 'rusiprisie@gmail.com', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bpjs`
--
ALTER TABLE `bpjs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `day_off`
--
ALTER TABLE `day_off`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poli`
--
ALTER TABLE `poli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue_list`
--
ALTER TABLE `queue_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bpjs`
--
ALTER TABLE `bpjs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `day_off`
--
ALTER TABLE `day_off`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `poli`
--
ALTER TABLE `poli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `queue_list`
--
ALTER TABLE `queue_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
