-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2020 at 07:17 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `antrian_rs`
--

-- --------------------------------------------------------

--
-- Table structure for table `bpjs`
--

CREATE TABLE `bpjs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bpjs_number` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(4) NOT NULL,
  `poli_id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  `status` varchar(12) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `poli_id`, `name`, `phone_number`, `status`, `date_modified`) VALUES
(1, 1, 'Line 01, Untuk pelayanan Informasi ajah', '', 'deleted', '2020-06-06 16:36:01'),
(2, 2, 'Line 02, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:08'),
(3, 3, 'Line 03, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:12'),
(4, 4, 'Line 04, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:14'),
(5, 5, 'Line 05, Untuk pelayanan Pembayaran / Kasir', '', 'deleted', '2020-06-06 16:36:17'),
(8, 8, 'Dr. Subagyo', '02151', 'active', '2020-06-06 16:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

CREATE TABLE `poli` (
  `id` int(11) NOT NULL,
  `prefix_code` varchar(3) NOT NULL,
  `name` varchar(30) NOT NULL,
  `close_time` time NOT NULL,
  `max_visitor` int(4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`id`, `prefix_code`, `name`, `close_time`, `max_visitor`, `status`, `date_modified`) VALUES
(1, 'A', 'Pembayaran', '00:00:06', 50, 'deleted', '2020-06-06 16:35:37'),
(2, 'B', 'Customer Service', '00:00:50', 50, 'deleted', '2020-06-06 16:35:31'),
(3, 'C', 'Berkas', '00:00:50', 50, 'deleted', '2020-06-06 16:35:41'),
(7, 'C', 'Berkas', '00:00:50', 50, 'deleted', '2020-05-08 15:38:30'),
(8, 'PD', 'Penyakit Dalam', '01:00:00', 120, 'active', '2020-06-06 16:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `queue_list`
--

CREATE TABLE `queue_list` (
  `id` int(11) NOT NULL,
  `poli_id` int(4) NOT NULL,
  `doctor_id` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bpjs_id` int(11) NOT NULL,
  `queue_code` varchar(8) NOT NULL,
  `queue_number` int(11) NOT NULL,
  `visit_date` date NOT NULL,
  `register_date` datetime NOT NULL,
  `status` varchar(25) NOT NULL DEFAULT 'new',
  `start_datetime` datetime NOT NULL,
  `finish_datetime` datetime NOT NULL,
  `note` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `queue_list`
--

INSERT INTO `queue_list` (`id`, `poli_id`, `doctor_id`, `user_id`, `bpjs_id`, `queue_code`, `queue_number`, `visit_date`, `register_date`, `status`, `start_datetime`, `finish_datetime`, `note`) VALUES
(1, 2, 0, 8, 1, 'B1', 1, '2020-05-01', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(2, 2, 0, 8, 1, 'B2', 2, '2020-05-01', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(3, 2, 0, 8, 1, 'B3', 3, '2020-05-01', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(4, 2, 0, 8, 1, 'B4', 4, '2020-05-01', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(5, 2, 0, 8, 1, 'B5', 5, '2020-04-18', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(6, 2, 0, 8, 1, 'B001', 1, '2020-04-20', '0000-00-00 00:00:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, 2, 0, 8, 1, 'B002', 2, '2020-04-20', '2020-04-18 12:12:02', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(8, 2, 0, 8, 1, 'B2', 2, '2020-04-20', '2020-04-18 12:12:59', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(9, 2, 0, 8, 1, '000', 2, '2020-04-20', '2020-04-18 12:14:09', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(10, 2, 1, 8, 1, 'B002', 2, '2020-04-18', '2020-04-18 12:14:55', 'start', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(11, 2, 0, 8, 1, 'B002', 2, '2020-04-20', '2020-04-18 12:15:16', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(12, 2, 0, 8, 1, 'B002', 2, '2020-04-20', '2020-04-18 12:15:25', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(13, 2, 0, 8, 1, 'B003', 3, '2020-04-20', '2020-04-18 12:15:59', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(14, 2, 0, 8, 1, 'B004', 4, '2020-04-20', '2020-04-18 12:16:04', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(15, 3, 1, 8, 1, 'C001', 1, '2020-05-09', '2020-05-07 11:12:54', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(16, 2, 0, 8, 1, 'B001', 1, '2020-05-09', '2020-05-07 11:15:15', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(17, 2, 0, 8, 1, 'B001', 1, '2020-05-07', '2020-05-07 11:17:48', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(18, 3, 2, 8, 1, 'C002', 2, '2020-05-09', '2020-05-07 11:12:54', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(19, 3, 1, 8, 1, 'C003', 3, '2020-05-09', '2020-05-07 11:12:54', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(20, 3, 1, 8, 1, 'C004', 4, '2020-05-09', '2020-05-07 11:12:54', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(21, 1, 0, 8, 1, 'A01', 1, '2020-05-21', '2020-05-10 18:23:48', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(22, 3, 0, 8, 1, 'C01', 1, '2020-05-10', '2020-05-10 18:25:06', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(23, 2, 0, 8, 1, 'B01', 1, '2020-05-11', '2020-05-10 18:35:09', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(24, 3, 0, 8, 1, 'C01', 1, '2020-05-12', '2020-05-10 18:39:00', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(25, 2, 0, 8, 1, 'B02', 2, '2020-05-11', '2020-05-10 18:41:39', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(26, 3, 0, 8, 1, 'C03', 3, '2020-05-11', '2020-05-10 18:42:51', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(27, 3, 0, 8, 1, 'C001', 1, '2020-05-20', '2020-05-10 18:44:09', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(28, 3, 0, 8, 1, 'C002', 2, '2020-05-21', '2020-05-10 18:46:15', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(29, 3, 0, 8, 1, 'C001', 1, '2020-05-27', '2020-05-10 18:47:38', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(30, 3, 0, 8, 1, 'C004', 4, '2020-05-11', '2020-05-10 18:48:41', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(31, 3, 0, 8, 1, 'C001', 1, '2020-05-17', '2020-05-17 15:47:26', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(32, 3, 0, 8, 1, 'C001', 1, '2020-05-18', '2020-05-17 15:47:39', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(33, 3, 0, 8, 1, 'C002', 2, '2020-05-18', '2020-05-17 15:48:11', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(34, 3, 0, 8, 1, 'C003', 3, '2020-05-18', '2020-05-17 15:48:34', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(35, 3, 0, 8, 1, 'C004', 4, '2020-05-18', '2020-05-17 15:48:52', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(36, 3, 0, 8, 1, 'C005', 5, '2020-05-18', '2020-05-17 15:49:06', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(37, 3, 0, 8, 1, 'C006', 6, '2020-05-18', '2020-05-17 15:49:46', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(38, 3, 0, 8, 1, 'C007', 7, '2020-05-18', '2020-05-17 15:50:03', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(39, 3, 0, 8, 1, 'C008', 8, '2020-05-18', '2020-05-17 15:50:27', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(40, 3, 0, 8, 1, 'C009', 9, '2020-05-18', '2020-05-17 15:50:55', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(41, 3, 0, 8, 1, 'C010', 10, '2020-05-18', '2020-05-17 15:51:40', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(42, 3, 0, 8, 1, 'C011', 11, '2020-05-18', '2020-05-17 15:51:58', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(43, 3, 0, 8, 1, 'C012', 12, '2020-05-18', '2020-05-17 15:52:22', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(44, 3, 0, 8, 1, 'C013', 13, '2020-05-18', '2020-05-17 15:52:43', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(45, 3, 0, 8, 1, 'C014', 14, '2020-05-18', '2020-05-17 15:53:08', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(46, 1, 0, 8, 1, 'A001', 1, '2020-05-18', '2020-05-17 16:02:14', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(47, 3, 0, 8, 1, 'C001', 1, '2020-06-08', '2020-06-01 09:29:14', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(48, 3, 0, 8, 1, 'C001', 1, '2020-06-18', '2020-06-01 09:35:26', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(49, 3, 0, 8, 1, 'C001', 1, '2020-06-19', '2020-06-01 09:36:01', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(50, 2, 0, 8, 1, 'B001', 1, '2020-06-09', '2020-06-01 09:39:25', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(51, 2, 0, 8, 1, 'B001', 1, '2020-06-10', '2020-06-01 09:40:34', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(52, 2, 0, 8, 1, 'B001', 1, '2020-06-11', '2020-06-01 09:45:19', 'new', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `level` varchar(25) NOT NULL,
  `last_login` datetime NOT NULL,
  `name` varchar(50) NOT NULL,
  `obsolete` tinyint(4) NOT NULL DEFAULT 0,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `last_login`, `name`, `obsolete`, `email`, `phone_number`) VALUES
(1, 'sa', 'be03ccaa6c0cbe1143cd6b8df4fa0a3726e34d3188da0e1668f05f8bd9d175669e6bc82284d4c2d25ec470f377eb5ecfe7926353700dbcec906621674c06bf7cfSqjntJ6tni82OFjrKd18N5wF5xE9Qx2lEigA718pao=', 'sa', '2020-06-06 18:33:20', 'System Administrator', 0, '', '0'),
(2, 'user', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'user', '2020-04-19 12:21:14', 'User', 0, '', '0'),
(3, 'op01', '73826ed17dfaaf31d611250455d840c9aa1113d70c1338f588d63a2d78d0eb91a684ccbcf046eae816c85ef473c5a9b797cc1ad128e52a3d31953540e6565739PUzFuiOPhO5GIlzVP8RylGPVhJIieom5xy6zZ5qa8pc=', 'operator', '2020-05-17 14:57:20', 'Jhoni', 0, '', '0'),
(4, 'op02', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'operator', '2020-05-07 12:15:21', 'Rina', 0, '', '0'),
(5, 'op03', '6c3fc37baf0057128cc553ae30c6434d62ad64f670ebba1fce9e6a09fe3d530b8570d126c07a243666617901e61de6f05381d73f682adca883a675fab1820d96YHoPhoMj+AiQg86HZ48OlNSnq/Q5bxmCgfz/QI0XkAU=', 'operator', '2020-04-04 04:28:59', 'Sabeni', 0, '', '0'),
(8, 'aprigeuza@gmail.com', '6a44c2bc978f9bd432711b923d96ad4cf67292821ab0e977c1e0f72750d810b2d10cf4a062e6fa586cd15f293c561ec2560230425103fd1157a57159d436fcea5CkK+SrWLcIlXHsG+sG36VziO21aNvlUTI7BOysurJk=', 'user', '2020-06-06 15:33:08', 'Apri', 0, 'aprigeuza@gmail.com', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bpjs`
--
ALTER TABLE `bpjs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `poli`
--
ALTER TABLE `poli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queue_list`
--
ALTER TABLE `queue_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bpjs`
--
ALTER TABLE `bpjs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `poli`
--
ALTER TABLE `poli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `queue_list`
--
ALTER TABLE `queue_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
