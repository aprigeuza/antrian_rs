VISI :

Berkualitas Prima Dengan Sentuhan Nurani

MISI :

Meningkatkan kompetensi sumberdaya manusia di segala lini pelayanan, baik pelayanan medis maupun manajemen, didukung oleh sarana dan prasarana yang memadai, serta manajemen yang efektif dan efisien.
Memberikan pelayanan kesehatan individu yang berkualitas, menyenangkan dan santun.
Mendukung rencana pembangunan daerah dalam meningkatkan derajat kesehatan masyarakat di Kabupaten Tangerang.