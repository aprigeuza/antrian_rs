<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config["user_level"] = array(
  "sa" => "Administrator",
  "operator" => "Operator",
  "doc_checking" => "Pemeriksa Dokumen",
  "user" => "User"
);

$config["admin_theme"] = "skin-red-light";
