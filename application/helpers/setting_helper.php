<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_setting'))
{
	function get_setting($name)
	{
		if(is_null($name)) return;

		$CI =& get_instance();
		$CI->load->model("setting_model");
		return $CI->setting_model->get_value($name);
	}
}

if ( ! function_exists('get_config'))
{
	function get_config($name)
	{
		if(is_null($name)) return;

		$CI =& get_instance();
    $CI->load->config("app_config");

		return $CI->config->item($name);
	}
}
