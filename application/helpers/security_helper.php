<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('validate_login_session'))
{
	function validate_login_session()
	{
		$CI =& get_instance();
		$CI->load->model("login_model");
		$check = $CI->login_model->validate_login_session();

    $u = $CI->uri->segment(1);

    switch ($u) {
      case 'admin':
        if(!in_array($_SESSION["auth_level"], ["sa", "doc_checking"]))
        {
          $check = false;
        }
      break;
      case 'operator':
        if(!in_array($_SESSION["auth_level"], ["operator"]))
        {
          $check = false;
        }
      break;
      case 'user':
        if(!in_array($_SESSION["auth_level"], ["user"]))
        {
          $check = false;
        }
      break;
      default:
        $check = false;
      break;
    }

		if($check==false) redirect ($u . "/login");

		return $check;
	}
}
