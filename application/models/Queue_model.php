<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue_model extends CI_Model{
  public $queue_code = "";
  public $queue_id = 0;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function save($data)
  {
    $data["queue_number"] = 0;
    $data["queue_code"] = "";
    $data["register_date"] = date("Y-m-d H:i:s");
    $data["status"] = "new";

    // Validasi Tanggal Registrasi
    $tgl1 = date("Y-m-d");
    $max_date = date('Y-m-d', strtotime('+6 days', strtotime($tgl1)));
    $visit_date = date("W", strtotime($data["visit_date"]));

    if (strtotime ($visit_date) > strtotime ($max_date))
    {
      return "diff_week";
    }


    $s = $this->db->get_where("poli", ["id" => $data["poli_id"]]);
    $s = $s->row();

    $prefix_code = $s->prefix_code;

    // Get Last Number
    $q = $this->db->get_where("queue_list", ["visit_date" => $data["visit_date"], "poli_id" => $data["poli_id"]]);

    if($q->num_rows() == 0)
    {
      $queue_code = queue_code($prefix_code, 1);
      $data["queue_number"] = 1;
      $data["queue_code"] = $queue_code;
    }
    else
    {

      $this->db->from("queue_list");
      $this->db->where("poli_id", $data["poli_id"]);
      $this->db->where("visit_date", $data["visit_date"]);
      $this->db->order_by("queue_number", "DESC");
      $this->db->limit(1);
      $q2 = $this->db->get();

      $q2 = $q2->row();

      $queue_number = $q2->queue_number + 1;
      $queue_code = queue_code($prefix_code, $queue_number);

      $data["queue_number"] = $queue_number;
      $data["queue_code"] = $queue_code;
    }

    // Day Off
    $do = $this->db->get_where("day_off", [
      "day_off_date" => $data["visit_date"],
      "poli_id" => $data["poli_id"],
      "doctor_id" => $data["doctor_id"]
    ]);

    if($do->num_rows())
    {
      return "day_off";
    }

    // Check date
    if($data["visit_date"] == date("Y-m-d"))
    {
      $H = (int)date("H");
      $i = (int)date("i");

      $ct = explode(":", $s->close_time);

      $_H = (int)$ct[0];
      $_i = (int)$ct[1];
      if($H > $_H)
      {
        return "max_time";
      }
      elseif($H == $_H)
      {
        if($i > $_i)
        {
          return "max_time";
        }
      }

    }

    // Max Per Day
    $max = $s->max_visitor;
    if($q->num_rows() >= $max)
    {
      return "max_visitor";
    }

    $res = $this->db->get_where("queue_list", ["visit_date" => $data["visit_date"], "user_id" => $data["user_id"], "bpjs_id" => $data["bpjs_id"]]);
    if($res->num_rows())
    {
      return "exists";
    }

    $this->db->from("queue_list");
    $this->db->where_in("status", ["new", "onprogress"]);
    $this->db->where("visit_date >=", $data["visit_date"]);
    $this->db->where("user_id", $data["user_id"]);
    $this->db->where("bpjs_id", $data["bpjs_id"]);
    $res = $this->db->get();
    if($res->num_rows())
    {
      return "on_antrian";
    }

    $this->db->insert("queue_list", $data);

    $c = $this->db->insert_id();

    $this->queue_number = $data["queue_code"];
    $this->queue_id = $c;

    if(!empty($c) && $c !== null)
    {
      return "ok";
    }
  }

  public function get_queue_number()
  {
    return $this->queue_number;
  }

  public function get_queue_id()
  {
    return $this->queue_id;
  }

  public function current()
  {

    $poli = $this->db->get_where("poli", ["status" => "active"]);

    foreach($poli->result() as $s)
    {
      $this->db->select("queue_list.*, doctor.name");
      $this->db->from("queue_list");
      $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "INNER");
      $this->db->join("user", "user.id=queue_list.user_id", "INNER");
      $this->db->where("queue_list.status", "start");
      $this->db->where("queue_list.visit_date", date("Y-m-d"));
      $this->db->where("queue_list.poli_id", $s->id);
      $res = $this->db->get();

      $data = array();

      foreach($res->result() as $row)
      {
        $_row = array();

        $_row["queue_code"];
        $_row["queue_number"];
        $_row["queue_poli"];
      }
    }
  }

  public function get_by_visit_date($visit_date)
  {
    $this->db->select("
    queue_list.*,
    poli.name AS poli_name,
    doctor.name AS doctor_name,
    doctor.phone_number AS doctor_phone_number,
    bpjs.bpjs_number,
    bpjs.name AS bpjs_name,
    user.name AS user_name,
    user.email AS user_email,
    user.phone_number AS user_phone_number
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->where("queue_list.visit_date", $visit_date);
    $res = $this->db->get();

    return $res;
  }

  function get_my_queue($id=0)
  {
    $user_id = get_session("auth_id");
    $this->db->select("
    queue_list.*,
    poli.name AS poli_name,
    doctor.name AS doctor_name,
    doctor.phone_number AS doctor_phone_number,
    bpjs.bpjs_number,
    bpjs.name AS bpjs_name,
    user.name AS user_name,
    user.email AS user_email,
    user.phone_number AS user_phone_number
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->where("queue_list.user_id", $user_id);
    if($id !== 0) $this->db->where("queue_list.id", $id);

    $this->db->order_by("queue_list.visit_date", "DESC");
    $res = $this->db->get();

    return $res;
  }


  function _my_queue($id, $status)
  {
    $user_id = get_session("auth_id");
    $this->db->select("
    queue_list.*,
    poli.name AS poli_name,
    doctor.name AS doctor_name,
    doctor.phone_number AS doctor_phone_number,
    bpjs.bpjs_number,
    bpjs.name AS bpjs_name,
    user.name AS user_name,
    user.email AS user_email,
    user.phone_number AS user_phone_number
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->where("queue_list.user_id", $user_id);
    if($id !== 0) $this->db->where("queue_list.id", $id);
    $this->db->where("queue_list.status", $status);

    $this->db->order_by("queue_list.visit_date", "DESC");
    $res = $this->db->get();

    return $res;
  }

  public function get_my_queue_list($id = 0)
  {


    $res = array();

    $res["result"] = array();

    $res["num_rows"] = 0;


    $r = $this->_my_queue($id, "new");
    foreach($r->result() as $row)
    {
      $res["result"][] = $row;
      $res["num_rows"] = $res["num_rows"] + 1;
    }

    $r = $this->_my_queue($id, "active");
    foreach($r->result() as $row)
    {
      $res["result"][] = $row;
      $res["num_rows"] = $res["num_rows"] + 1;
    }

    $r = $this->_my_queue($id, "on_antrian");
    foreach($r->result() as $row)
    {
      $res["result"][] = $row;
      $res["num_rows"] = $res["num_rows"] + 1;
    }

    $r = $this->_my_queue($id, "finish");
    foreach($r->result() as $row)
    {
      $res["result"][] = $row;
      $res["num_rows"] = $res["num_rows"] + 1;
    }

    $r = $this->_my_queue($id, "cancel");
    foreach($r->result() as $row)
    {
      $res["result"][] = $row;
      $res["num_rows"] = $res["num_rows"] + 1;
    }


    return (object)$res;
  }

  public function get_in_queue($visit_date, $poli_id)
  {
    $this->db->select("
    queue_list.*,
    poli.name AS poli_name,
    doctor.name AS doctor_name,
    doctor.phone_number AS doctor_phone_number,
    bpjs.bpjs_number,
    bpjs.name AS bpjs_name,
    user.name AS user_name,
    user.email AS user_email,
    user.phone_number AS user_phone_number,
    op.name AS operator_name,
    ve.name AS verificator_name
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->join("user AS op", "op.id=queue_list.operator_id", "LEFT");
    $this->db->join("user AS ve", "ve.id=bpjs.verificator_id", "LEFT");
    $this->db->where("queue_list.status", "new");
    $this->db->where("queue_list.visit_date", $visit_date);
    $this->db->where("queue_list.poli_id", $poli_id);
    $this->db->order_by("queue_list.queue_code", "ASC");
    $res = $this->db->get();

    return $res;
  }

  public function cancel($id)
  {
    $where["id"] = $id;
    $data["status"] = "cancel";
    $this->db->update("queue_list", $data, $where);
    return true;
  }

  public function get_active($poli_id)
  {
    $now = date("Y-m-d");

    $this->db->from("queue_list");
    $this->db->where("visit_date", $now);
    $this->db->where("poli_id", $poli_id);
    $this->db->where("status", "active");
    $this->db->order_by("queue_code", "DESC");
    $res = $this->db->get();
    if($res->num_rows())
    {
      return $res->row();
    }
    return null;
  }

  public function r_total_today()
  {
    $res = $this->db->get_where("queue_list", ["visit_date" => date("Y-m-d")]);
    return $res->num_rows();
  }

  public function r_total_week()
  {
    $ddate = date("Y-m-d");
    $date = new DateTime($ddate);
    $week = $date->format("W");

    $res = $this->db->get_where("queue_list", ["WEEK(visit_date)" => $week]);
    return $res->num_rows();
  }

  public function r_total_month()
  {
    $res = $this->db->get_where("queue_list", ["MONTH(visit_date)" => date("m")]);
    return $res->num_rows();
  }

  public function get_by_period($date_start, $date_end, $status)
  {
    $this->db->select("
    queue_list.*,
    poli.name AS poli_name,
    doctor.name AS doctor_name,
    doctor.phone_number AS doctor_phone_number,
    bpjs.bpjs_number,
    bpjs.name AS bpjs_name,
    user.name AS user_name,
    user.email AS user_email,
    user.phone_number AS user_phone_number,
    op.name AS operator_name,
    ve.name AS verificator_name
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->join("user AS op", "op.id=queue_list.operator_id", "LEFT");
    $this->db->join("user AS ve", "ve.id=bpjs.verificator_id", "LEFT");
    $this->db->group_start();
    $this->db->where("queue_list.visit_date >=", $date_start);
    $this->db->where("queue_list.visit_date <=", $date_end);
    $this->db->group_end();
    if($status != "all") $this->db->where("queue_list.status", $status);
    $this->db->order_by("queue_list.visit_date", "ASC");
    $this->db->order_by("poli.name", "ASC");
    $this->db->order_by("queue_list.queue_code", "ASC");
    $res = $this->db->get();

    return $res;
  }

  public function get_for_line_chart($status, $year)
  {
    $data = array();
    for($m=1; $m <= 12; $m++)
    {
      $this->db->from("queue_list");
      $this->db->where("YEAR(queue_list.visit_date)", $year);
      $this->db->where("MONTH(queue_list.visit_date)", $m);
      $this->db->where("queue_list.status", $status);
      $res = $this->db->get();

      $data[] = $res->num_rows();
    }

    return $data;
  }

  public function cancel_q()
  {
    $this->db->update("queue_list", ["status" => "cancel"], ["visit_date <", date("Y-m-d"), "status" => "new"]);
  }

}
