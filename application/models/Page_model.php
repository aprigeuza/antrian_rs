<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("page", $data);

      $insert_id = $this->db->insert_id();

      return $insert_id;
    }
    else
    {
      $this->db->update("page", $data, array("id" => $id));

      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      return $this->db->get("page");
    }
    else
    {
      $row = $this->db->get_where("page", array("id" => $id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->db->delete("page", array("id" => $id));

    return true;
  }

  public function set_status($status, $id)
  {
    $this->db->update("page", array("status" => $status, "date_modified" => date("Y-m-d H:i:s")), array("id" => $id));
    return $id;
  }

}
