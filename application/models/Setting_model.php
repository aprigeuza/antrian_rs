<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_value($name)
	{
		$res = $this->db->get_where("setting", array("name" => $name));
		if($res->num_rows()>=1)
		{
			return $res->row()->value;
		}
		return NULL;
	}

  public function get_data($name)
  {
		$res = $this->db->get_where("setting", array("name" => $name));
		if($res->num_rows()>=1)
		{
			return $res->row();
		}
		return NULL;
  }

	public function update($name, $value)
	{
		$this->db->update("setting", array("value" => $value), array("name" => $name));
	}

  public function delete($name)
  {
    $this->db->delete("setting", array("name" => $name));
  }

	public function insert($name, $value)
	{
		$this->db->insert("setting", array("value" => $value, "name" => $name));
	}

  public function replace($data)
  {
    $this->db->replace("setting", $data);
  }
}
