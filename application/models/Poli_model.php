<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poli_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("poli", $data);

      $insert_id = $this->db->insert_id();

      $this->db->update("poli", array("date_modified" => date("Y-m-d H:i:s")), array("id" => $insert_id));

      return $insert_id;
    }
    else
    {
      $this->db->update("poli", $data, array("id" => $id));

      $this->db->update("poli", array("date_modified" => date("Y-m-d H:i:s")), array("id" => $id));

      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      $this->db->where("status !=", "deleted");
      return $this->db->get("poli");
    }
    else
    {
      $row = $this->db->get_where("poli", array("id" => $id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->set_status("deleted", $id);
    return true;
  }

  public function set_status($status, $id)
  {
    $this->db->update("poli", array("status" => $status, "date_modified" => date("Y-m-d H:i:s")), array("id" => $id));
    return $id;
  }

  public function get_active()
  {
    $this->db->select("*");
    $this->db->from("poli");
    $this->db->where("status", "active");
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

}
