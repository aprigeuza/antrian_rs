<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function email_exists($email)
  {
    $this->db->from("user");
    $this->db->where("email", $email);
    $r = $this->db->get();

    if($r->num_rows()) return 1;

    return 0;
  }

  public function save($data)
  {
    $this->db->insert("user", $data);

    // Set Login
    $this->login($data["email"], $data["password"]);
  }

  public function login($email, $password)
  {
    $this->db->from("user");
    $this->db->where("email", $email);
    $validate = $this->db->get();

    if($validate->num_rows() == 1)
    {
      $row = $validate->row();
      $decrypt_password = $this->encryption->decrypt($password);
      $decrypt_row_password = $this->encryption->decrypt($row->password);

      if($decrypt_row_password === $decrypt_password)
      {
        // Create session
        $r = $validate->row();

        $this->session->set_userdata("auth_id", $r->id);
        $this->session->set_userdata("auth_username", $r->username);
        $this->session->set_userdata("auth_email", $r->email);
        $this->session->set_userdata("auth_name", $r->name);
        $this->session->set_userdata("auth_password", $r->password);
        $this->session->set_userdata("auth_level", $r->level);
        $this->session->set_userdata("auth_last_login", date("Y-m-d H:i:s"));

        $this->db->update("user", array("last_login" => date("Y-m-d H:i:s")), array("email" => $r->email));

        return true;
      }
      else
      {
        return false;
      }
    }
    return false;
  }

}
