<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function data()
  {
    return $this->db->get("user");
  }

  public function get_data($id)
  {
    $res = $this->db->get_where("user", array("id" => $id));

    if($res->num_rows()) return $res->row();

    return false;
  }

  public function insert($data)
  {
    $this->db->insert("user", $data);
  }

  public function update($data, $id)
  {
    $this->db->update("user", $data, array("id" => $id));
  }

  public function delete($id)
  {
    $this->db->delete("user", array("id" => $id));
  }

  public function user_exists($email, $level)
  {
    $this->db->select();
    $this->db->from("user");
    $this->db->where("level", $level);
    $this->db->where("email", $email);
    $res = $this->db->get();

    if($res->num_rows()) return true;
    return false;
  }

}
