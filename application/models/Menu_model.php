<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("menu", $data);

      $insert_id = $this->db->insert_id();

      return $insert_id;
    }
    else
    {
      $this->db->update("menu", $data, array("id" => $id));

      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      return $this->db->get("menu");
    }
    else
    {
      $row = $this->db->get_where("menu", array("id" => $id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->db->delete("menu", array("id" => $id));

    return true;
  }

}
