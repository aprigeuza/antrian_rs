<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpjs_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function data_user()
  {
    $user_id = get_session("auth_id");
    $this->db->from("bpjs");
    $this->db->where("user_id", $user_id);
    return $this->db->get();
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("bpjs", $data);

      $insert_id = $this->db->insert_id();

      return $insert_id;
    }
    else
    {
      $this->db->update("bpjs", $data, array("id" => $id));

      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      $user_id = get_session("auth_id");
      $this->db->where("user_id", $user_id);
      $this->db->where("status !=", "deleted");
      return $this->db->get("bpjs");
    }
    else
    {
      $user_id = get_session("auth_id");
      $row = $this->db->get_where("bpjs", array("id" => $id, "user_id" => $user_id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->set_status("deleted", $id);
    return true;
  }

  public function set_status($status, $id)
  {
    $this->db->update("bpjs", array("status" => $status, "date_modified" => date("Y-m-d H:i:s")), array("id" => $id));
    return $id;
  }

  public function get_active()
  {
    $this->db->select("*");
    $this->db->from("bpjs");
    $this->db->where("status", "active");
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

  public function get_verified_by_user()
  {
    $this->db->select("*");
    $this->db->from("bpjs");
    $this->db->where("status", "active");
    $this->db->where("verification_status", "ok");
    $this->db->where("user_id", get_session("auth_id"));
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

  public function get_verified()
  {
    $this->db->select("*");
    $this->db->from("bpjs");
    $this->db->where("status", "active");
    $this->db->where("verification_status", "ok");
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

  public function get_all()
  {
    $this->db->select("*");
    $this->db->from("bpjs");
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

  public function get_outstanding()
  {
    $this->db->select("*");
    $this->db->from("bpjs");
    $this->db->where("status", "active");
    $this->db->where("verification_status", "new");
    $this->db->order_by("name", "ASC");
    return $this->db->get();
  }

  public function get_detail($id)
  {
    $row = $this->db->get_where("bpjs", array("id" => $id))->row();
    return $row;
  }

  public function verification($id)
  {
    $this->db->update("bpjs", ["verification_status" => "ok", "verificator_id" => get_session("auth_id")], ["id" => $id]);
  }

  public function reject($id, $verification_info)
  {
    $this->db->update("bpjs", ["verification_status" => "reject", "verificator_id" => get_session("auth_id"), "verification_info" => $verification_info], ["id" => $id]);
  }

}
