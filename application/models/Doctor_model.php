<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("doctor", $data);

      $insert_id = $this->db->insert_id();

      $this->db->update("doctor", array("date_modified" => date("Y-m-d H:i:s")), array("id" => $insert_id));

      return $insert_id;
    }
    else
    {
      $this->db->update("doctor", $data, array("id" => $id));

      $this->db->update("doctor", array("date_modified" => date("Y-m-d H:i:s")), array("id" => $id));

      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      $this->db->select("doctor.*, poli.name AS poli_name");
      $this->db->where("doctor.status !=", "deleted");
      $this->db->join("poli", "poli.id=doctor.poli_id", "LEFT");
      return $this->db->get("doctor");
    }
    else
    {
      $this->db->select("doctor.*, poli.name AS poli_name");
      $this->db->where("doctor.status !=", "deleted");
      $this->db->join("poli", "poli.id=doctor.poli_id", "LEFT");
      $row = $this->db->get_where("doctor", array("doctor.id" => $id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->set_status("deleted", $id);
    return true;
  }

  public function set_status($status, $id)
  {
    $this->db->update("doctor", array("status" => $status, "date_modified" => date("Y-m-d H:i:s")), array("id" => $id));
    return $id;
  }

  public function get_active()
  {
    $this->db->select("*");
    $this->db->from("doctor");
    $this->db->where("status", "active");
    return $this->db->get();
  }

  public function get_active_by_poli($poli_id)
  {
    $this->db->select("*");
    $this->db->from("doctor");
    $this->db->where("status", "active");
    $this->db->where("poli_id", $poli_id);
    return $this->db->get();
  }

}
