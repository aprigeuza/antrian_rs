<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display_model extends CI_Model{

  private $_poli;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->_poli = $this->session->userdata("poli_id");
  }
  public function get_poli()
  {
    return $this->db->get_where("poli", ["id" => $this->_poli])->row();
  }

  public function get_active_queue()
  {
    $this->db->select("*");
    $this->db->from("queue_list");
    $this->db->where("poli_id", $this->_poli);
    $this->db->where("status", "active");
    $this->db->where("visit_date", date("Y-m-d"));
    $res = $this->db->get();

    return $res;
  }

  public function get_total()
  {
    $this->db->select("*");
    $this->db->from("queue_list");
    $this->db->where("poli_id", $this->_poli);
    $this->db->where("status !=", "cancel");
    $this->db->where("visit_date", date("Y-m-d"));
    $res = $this->db->get();

    return $res->num_rows();
  }

  public function get_queue_list()
  {
    $this->db->select("*");
    $this->db->from("queue_list");
    $this->db->where("poli_id", $this->_poli);
    $this->db->where("visit_date", date("Y-m-d"));
    $this->db->group_start();
    $this->db->where("status", "new");
    $this->db->or_where("status", "active");
    $this->db->group_end();
    $res = $this->db->get();

    return $res;
  }

}
