<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function validate_login_session()
  {
    $username = $this->session->userdata("auth_username");
    $password = $this->session->userdata("auth_password");

    $validate = $this->db->get_where("user", array("username" => $username));

    if($validate->num_rows() == 1)
    {
      $row = $validate->row();

      $decrypt_row_password = $this->encryption->decrypt($row->password);
      $decrypt_ses_password = $this->encryption->decrypt($password);
      if($decrypt_row_password === $decrypt_ses_password)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    return false;
  }

  public function check_login($username, $password)
  {
    $this->db->from("user");
    $this->db->where("username", $username);
    $validate = $this->db->get();

    if($validate->num_rows() == 1)
    {
      $row = $validate->row();
      $decrypt_row_password = $this->encryption->decrypt($row->password);

      if($decrypt_row_password === $password)
      {
        // Create session
        $r = $validate->row();

        $this->session->set_userdata("auth_id", $r->id);
        $this->session->set_userdata("auth_username", $r->username);
        $this->session->set_userdata("auth_name", $r->name);
        $this->session->set_userdata("auth_email", $r->email);
        $this->session->set_userdata("auth_phone_number", $r->auth_phone_number);
        $this->session->set_userdata("auth_password", $r->password);
        $this->session->set_userdata("auth_level", $r->level);
        $this->session->set_userdata("auth_last_login", date("Y-m-d H:i:s"));

        $this->db->update("user", array("last_login" => date("Y-m-d H:i:s")), array("username" => $r->username));

        return true;
      }
      else
      {
        return false;
      }
    }
    return false;
  }

  public function logout()
  {
    $this->session->unset_userdata("auth_id");
    $this->session->unset_userdata("auth_username");
    $this->session->unset_userdata("auth_password");
    $this->session->unset_userdata("auth_level");
    $this->session->unset_userdata("auth_last_login");

    $this->session->sess_destroy();

    session_destroy();
    return true;
  }

}
