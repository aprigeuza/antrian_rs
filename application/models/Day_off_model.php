<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Day_off_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function save($data, $id = 0)
  {
    if($id == 0)
    {
      $this->db->insert("day_off", $data);
      $insert_id = $this->db->insert_id();
      return $insert_id;
    }
    else
    {
      $this->db->update("day_off", $data, array("id" => $id));
      return $id;
    }
  }

  public function data($id = 0)
  {
    if($id == 0)
    {
      $this->db->select("day_off.*, poli.name AS poli_name, doctor.name AS doctor_name");
      $this->db->join("poli", "poli.id=day_off.poli_id", "LEFT");
      $this->db->join("doctor", "doctor.id=day_off.doctor_id", "LEFT");
      return $this->db->get("day_off");
    }
    else
    {
      $this->db->select("day_off.*, poli.name AS poli_name, doctor.name AS doctor_name");
      $this->db->join("poli", "poli.id=day_off.poli_id", "LEFT");
      $this->db->join("doctor", "doctor.id=day_off.doctor_id", "LEFT");
      $row = $this->db->get_where("day_off", array("day_off.id" => $id))->row();
      return $row;
    }
  }

  public function delete($id)
  {
    $this->db->delete("day_off", ["id" => $id]);
    return true;
  }

  public function get_jadwal()
  {
    $week = date("W", strtotime(date("Y-m-d")));
    $year = date("Y");
    $week_days = function($week, $year){
      $dto = new DateTime();
      $dto->setISODate($year, $week);
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      $dto->modify('+1 days');
      $ret[] = $dto->format('Y-m-d');
      return $ret;
    };

    $check_day_off = function($doctor_id, $date){
      $CI =& get_instance();
      $CI->db->from("day_off");
      $CI->db->where("doctor_id", $doctor_id);
      $CI->db->where("day_off_date", $date);
      $res = $CI->db->get();
      return $res;
    };

    $data = [];

    $this->db->select("doctor.id AS doctor_id, doctor.name AS doctor_name, poli.id AS poli_id, poli.name AS poli_name");
    $this->db->from("doctor");
    $this->db->join("poli", "poli.id=doctor.poli_id", "INNER");
    $this->db->where("doctor.status !=", "delted");
    $this->db->order_by("poli.name", "ASC");
    $doctors = $this->db->get();
    foreach($doctors->result() as $row)
    {
      $doctor["doctor_id"] = $row->doctor_id;
      $doctor["doctor_name"] = $row->doctor_name;
      $doctor["poli_name"] = $row->poli_name;

      $jadwal = [];
      foreach($week_days($week, $year) as $day)
      {
        $c = $check_day_off($row->doctor_id, $day);
        if($c->num_rows())
        {
          $cc = $c->row();
          $jadwal[] = (object) array(
            "date" => $day,
            "note" => $cc->note,
            "status" => "off"
          );
        }
        else
        {
          $jadwal[] = (object) array(
            "date" => $day,
            "note" => "",
            "status" => "available"
          );
        }
      }

      $doctor["jadwal"] = (object)$jadwal;

      $data[] = (object)$doctor;
    }

    return (object)$data;
  }
}
