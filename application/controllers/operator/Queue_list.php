<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue_list extends CI_Controller{

  var $queue_number = 0;
  var $poli_id = 0;
  var $service_line_id = 0;

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session


    $queue_number = $this->session->userdata("queue_number");
    if(!empty($queue_number))
    {
      $this->queue_number = $queue_number;
    }

    $poli_id = $this->session->userdata("poli_id");
    if(!empty($poli_id))
    {
      $this->poli_id = $poli_id;
    }
  }

  public function index()
  {

  }

  public function get_list()
  {
    $this->db->select("queue_list.id, queue_list.status, queue_list.queue_code, queue_list.queue_number, bpjs.name");
    $this->db->from("queue_list");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "INNER");
    // $this->db->where("queue_list.status", "new");
    $this->db->where("queue_list.visit_date", date("Y-m-d"));
    $this->db->where("queue_list.poli_id", $this->poli_id);
    $res = $this->db->get();

    $data = array();
    foreach($res->result() as $row)
    {
      $data[] = $row;
    }

    json_file(["data" => $data]);
  }

  public function qprev()
  {
    if(empty($this->queue_number))
    {
      json_file(["msg" => "Empty Row"]);
    }
    else
    {
      $this->db->select("*");
      $this->db->from("queue_list");
      $this->db->where("visit_date", date("Y-m-d"));
      $this->db->where("poli_id", $this->poli_id);
      $this->db->where("queue_number <", $this->queue_number);
      $this->db->order_by("queue_number", "DESC");
      $this->db->limit(1);
      $current = $this->db->get();

      if($current->num_rows() == 0)
      {
        // Set new after current active number
        $this->db->set("status", "new");
        $this->db->where("visit_date", date("Y-m-d"));
        $this->db->where("status !=", "cancel");
        $this->db->where("poli_id", $this->poli_id);
        $this->db->update("queue_list");

        $this->session->set_userdata("queue_number", 0);
        json_file(["status" => false]);
      }
      else
      {
        $current_row = $current->row();

        // Set new after current active number
        $this->db->set("status", "new");
        $this->db->where("visit_date", date("Y-m-d"));
        $this->db->where("status !=", "cancel");
        $this->db->where("poli_id", $this->poli_id);
        $this->db->where("queue_number > ", $current_row->queue_number);
        $this->db->update("queue_list");

        // Set Current Active
        $this->db->set("status", "active");
        $this->db->where("id", $current_row->id);
        $this->db->update("queue_list");
        $this->session->set_userdata("queue_number", $current_row->queue_number);
        json_file($current_row);
      }
    }
  }

  public function qnext()
  {
    if(!$this->queue_number)
    {
      $this->db->select("*");
      $this->db->from("queue_list");
      $this->db->where("status", "new");
      $this->db->where("visit_date", date("Y-m-d"));
      $this->db->where("poli_id", $this->poli_id);
      $this->db->order_by("queue_number", "ASC");
      $this->db->limit(1);
      $current = $this->db->get();

      if($current->num_rows())
      {
        $current_row = $current->row();

        $this->session->set_userdata("queue_number", $current_row->queue_number);

        // Set Current Active
        $this->db->set("status", "active");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("id", $current_row->id);
        $this->db->update("queue_list");

        // Set finish before current active number
        $this->db->set("status", "finish");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("visit_date", date("Y-m-d"));
        $this->db->where("status !=", "cancel");
        $this->db->where("poli_id", $this->poli_id);
        $this->db->where("queue_number < ", $current_row->queue_number);
        $this->db->update("queue_list");

        json_file($current->row());
      }
      else
      {
        $this->session->set_userdata("queue_number", 0);

        // Set finish before current active number
        $this->db->set("status", "finish");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("visit_date", date("Y-m-d"));
        $this->db->where("status !=", "cancel");
        $this->db->where("poli_id", $this->poli_id);
        $this->db->update("queue_list");

        json_file(["msg" => "Empty Row"]);
      }
    }
    else
    {
      $this->db->select("*");
      $this->db->from("queue_list");
      $this->db->where("status", "new");
      $this->db->where("visit_date", date("Y-m-d"));
      $this->db->where("poli_id", $this->poli_id);
      $this->db->where("queue_number >", $this->queue_number);
      $this->db->order_by("queue_number", "ASC");
      $this->db->limit(1);
      $res = $this->db->get();

      if($res->num_rows() == 0)
      {

        // Set finish last active number
        $this->db->set("status", "finish");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("poli_id", $this->poli_id);
        $this->db->update("queue_list");

        $this->session->set_userdata("queue_number", 0);
        json_file(["status" => false]);
      }
      else
      {
        $current_row = $res->row();

        // Set finish before current active number
        $this->db->set("status", "finish");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("visit_date", date("Y-m-d"));
        $this->db->where("status !=", "cancel");
        $this->db->where("poli_id", $this->poli_id);
        $this->db->where("queue_number < ", $current_row->queue_number);
        $this->db->update("queue_list");

        // Set Current Active
        $this->db->set("status", "active");
        $this->db->set("operator_id", get_session("auth_id"));
        $this->db->where("id", $current_row->id);
        $this->db->update("queue_list");

        $this->session->set_userdata("queue_number", $current_row->queue_number);

        json_file($current_row);
      }
    }
  }

  public function get_inf()
  {
    $id = $this->input->post_get("id");
    $this->db->select("
      queue_list.*,
      poli.name AS poli_name,
      doctor.name AS doctor_name,
      doctor.phone_number AS doctor_phone_number,
      bpjs.bpjs_number,
      bpjs.name AS bpjs_name,
      user.name AS user_name,
      user.email AS user_email,
      user.phone_number AS user_phone_number
    ");
    $this->db->from("queue_list");
    $this->db->join("poli", "poli.id=queue_list.poli_id", "LEFT");
    $this->db->join("doctor", "doctor.id=queue_list.doctor_id", "LEFT");
    $this->db->join("user", "user.id=queue_list.user_id", "LEFT");
    $this->db->join("bpjs", "bpjs.id=queue_list.bpjs_id", "LEFT");
    $this->db->where("queue_list.visit_date", date("Y-m-d"));
    $this->db->where("queue_list.status", "active");
    $this->db->where("queue_list.poli_id", $this->poli_id);
    $this->db->order_by("queue_number", "DESC");
    $res = $this->db->get();
    $row = $res->row();
    if($res->num_rows())
    {
      $this->session->set_userdata("queue_number", $row->queue_number);

      json_file(
        [
          "status" => true,
          "data" => $row
        ]
      );
    }
    else
    {
      $this->session->set_userdata("queue_number", 0);
      json_file(
        ["status" => false]
      );
    }
  }

}
