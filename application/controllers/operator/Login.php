<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model("poli_model");
  }

  public function index()
  {
    $data = array();

    $data["poli_list"] = $this->poli_model->get_active();
    $this->load->view("operator/login", $data);
  }

  public function validate()
  {
    // Set Session only
    $username = $this->input->post("username");
    $password = $this->input->post("password");
    $poli_id = $this->input->post("poli_id");

    // Check Login
    // ->check_login($username, $password);
    $check_login = $this->login_model->check_login($username, $password);

    if($check_login)
    {
      $poli_id = $this->session->set_userdata("poli_id", $poli_id);
      redirect("operator/dashboard");
    }
    else
    {
      set_alert("danger", "Auth failed, username/email or password wrong.");
      redirect("operator/login?alert=error_auth_failed");
    }
  }

  public function logout()
  {
    $logout = $this->login_model->logout();

    if($logout)
    {
      $this->session->sess_destroy();
      redirect("operator/login?alert=logout");
    }
    else
    {
      redirect("operator/404");
    }
  }
}
