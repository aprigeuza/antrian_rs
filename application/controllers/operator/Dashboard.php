<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("poli_model");
    $this->load->model("queue_model");
  }

  public function index()
  {
    $data = array();

    $poli_id = $this->session->userdata("poli_id");
    $data["poli"] = $this->poli_model->data($poli_id);
    $this->load->view("operator/dashboard/index", $data);
  }
}
