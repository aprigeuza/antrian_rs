<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    $this->load->model("display_model");
    $this->load->model("poli_model");
  }

  function index()
  {
    $data = array();
    $poli_id = $this->session->userdata("poli_id");
    $data["poli"] = $this->poli_model->data($poli_id);
    $this->load->view("operator/display/index", $data);
  }

  function get_result()
  {
    $active_queue = $this->display_model->get_active_queue();
    $total_rows  = $this->display_model->get_total();
    $queue_list  = $this->display_model->get_queue_list();

    $response = [];

    if($active_queue->num_rows())
    {
      $response["active_queue"] = $active_queue->row();
    }
    else
    {
      $response["active_queue"] = false;
    }

    $response["total_rows"] = $total_rows;

    if($queue_list->num_rows())
    {
      $response["queue_list"] = $queue_list->result();
    }
    else
    {
      $response["queue_list"] = false;
    }


    json_file($response);
  }

}
