<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    $this->load->model("queue_model");
    $this->load->model("poli_model");
    $this->load->model("doctor_model");
    $this->load->model("bpjs_model");
  }

  function index()
  {
    $data["queue_list"] = $this->queue_model->get_my_queue_list();

    $this->load->view("user/queue/index", $data);
  }

  public function register()
  {
    $data = array();
    $data["poli"] = $this->poli_model->get_active();
    $data["bpjs"] = $this->bpjs_model->get_verified_by_user();
    $this->load->view("user/queue/register", $data);
  }

  public function get_doctor()
  {
    $response = array();
    $poli_id = $this->input->post("poli_id");

    if($poli_id)
    {
      $doctor = $this->doctor_model->get_active_by_poli($poli_id);

      if($doctor->num_rows())
      {
        $data = array();

        foreach($doctor->result() as $row)
        {
          $r["id"] = $row->id;
          $r["name"] = $row->name;
          $data[] = $r;
        }

        $response["status"] = true;
        $response["data"] = $data;
        $response["message"] = "";
        json_file($response);
      }
      else
      {
        $response["status"] = true;
        $response["data"] = array();
        $response["message"] = "Data tidak ada";
        json_file($response);
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Pilih Poli";
      json_file($response);
    }
  }

  public function save()
  {
    $response = array();

    // Check Available
    $bpjs_id = $this->input->post("bpjs_id");
    $poli_id = $this->input->post("poli_id");
    $doctor_id = $this->input->post("doctor_id");
    $visit_date = $this->input->post("visit_date");


    if(empty($bpjs_id))
    {
      $response["status"] = false;
      $response["field"] = "bpjs_id";
      $response["message"] = "Harap Pilih BPJS!";
      json_file($response);
    }
    if(empty($poli_id))
    {
      $response["status"] = false;
      $response["field"] = "poli_id";
      $response["message"] = "Harap Pilih Poli!";
      json_file($response);
    }
    if(empty($doctor_id))
    {
      $response["status"] = false;
      $response["field"] = "doctor_id";
      $response["message"] = "Harap pilih doctor!";
      json_file($response);
    }
    if(empty($visit_date))
    {
      $response["status"] = false;
      $response["field"] = "visit_date";
      $response["message"] = "Tanggal Datang harus di isi!";
      json_file($response);
    }

    $data["user_id"] = get_session("auth_id");
    $data["bpjs_id"] = $bpjs_id;
    $data["poli_id"] = $poli_id;
    $data["doctor_id"] = $doctor_id;
    $data["visit_date"] = $visit_date;

    $doctor = $this->doctor_model->data($doctor_id);

    $queue = $this->queue_model;
    $save = $queue->save($data);

    switch ($save) {
      case 'diff_week':
      $response["status"] = false;
      $response["message"] = "Pendaftaran tidak bisa di lakukan lebih dari 7 hari dari tanggal hari ini!";
      json_file($response);
      break;
      case 'exists':
      $response["status"] = false;
      $response["message"] = "Anda sudah terdaftar untuk tanggal ".date("d/M/Y", strtotime($visit_date))." !";
      json_file($response);
      break;
      case 'on_antrian':
      $response["status"] = false;
      $response["message"] = "Nomor BPJS ini masih dalam Antrian!, Silahkan lihat di menu Antrian.";
      json_file($response);
      break;
      case 'max_time':
      $response["status"] = false;
      $response["message"] = "Antrian untuk hari ini sudah ditutup!";
      json_file($response);
      break;
      case 'max_visitor':
      $response["status"] = false;
      $response["message"] = "Antrian untuk tanggal ".date("d/M/Y", strtotime($visit_date))." sudah penuh!";
      json_file($response);
      break;
      case 'ok':

      // $this->load->library('email');
      // $config['smtp_host'] = 'imap.gmail.com';
      // $config['smtp_user'] = 'pendaftaranonlinebficikupa@gmail.com';
      // $config['smtp_pass'] = 'lp-ko0ji9';
      // $config['smtp_port'] = '465';
      // $config['smtp_crypto'] = 'ssl';
      // $config['mailtype'] = 'html';
      // $config['crlf'] = "\r\n";
      // $config['newline'] = "\r\n";
      // $config['charset'] = 'iso-8859-1';
      // $config['protocol'] = 'smtp';
      // $config['mailpath'] = "\"C:\xampp\sendmail\sendmail.exe\" -t";
      // $config['wordwrap'] = TRUE;
      // $this->email->initialize($config);
      //
      // $data["name"] = $name;
      // $data["email"] = $email;
      // $data["visit_date"] = $visit_date;
      // $data["contract_number"] = $contract_number;
      // $data["queue_number"] = $queue->get_queue_number();
      //
      // $mail_body2 = $this->load->view("register_email", $data, true);
      //
      // $to = $email;
      // $this->email->to($to);
      // $this->email->from('pendaftaranonlinebficikupa@gmail.com');
      // $this->email->subject('Pendaftaran Online BFI Cikupa');
      // $this->email->message($mail_body2);
      // $this->email->send();

      $response["status"] = true;
      $response["message"] = "Anda berhasil terdaftar untuk tanggal ".date("d/M/Y", strtotime($visit_date)).", dengan nomor ".$queue->get_queue_number().".!";
      $response["id"] = $queue->get_queue_id();
      json_file($response);
      break;
      case 'day_off':
      $response["status"] = false;
      $response["field"] = "visit_date";
      $response["message"] = "Mohon Maaf,untuk tanggal ".date("d/M/Y", strtotime($visit_date))." doktor \"".$doctor->name."\"  tidak tersedia, silahkan pilih tanggal lain!";
      json_file($response);
      break;
    }
  }

  public function cancel()
  {
    $id = $this->input->post("id");

    $my = $this->queue_model->get_my_queue($id)->row();

    if($my->status == "active" or $my->status == "finish")
    {
      $response["status"] = false;
      $response["message"] = "Antrian tidak bisa di batalkan.";
      json_file($response);
    }
    else
    {
      $this->queue_model->cancel($id);
      $response["status"] = true;
      $response["message"] = "Berhasil membatalkan Antrian.";
      json_file($response);
    }
  }

  public function get_active()
  {
    $poli_id = $this->input->post("poli_id");

    $a = $this->queue_model->get_active($poli_id);

    if($a == null)
    {
      $response["status"] = false;
      $response["message"] = "none";
      json_file($response);
    }
    else
    {
      $response["status"] = true;
      $response["data"] = $a;
      $response["message"] = "";
      json_file($response);
    }
  }

  public function in()
  {
    $data = array();
    $id = $this->input->get("id");
    $data["q"] = $this->queue_model->get_my_queue($id)->row();
    $this->load->view("user/queue/in", $data);
  }

  public function in_queue()
  {
    $queue_id = $this->input->get_post("queue_id");
    $visit_date = $this->input->get_post("visit_date");
    $poli_id = $this->input->get_post("poli_id");
    $data["queue_id"] = $queue_id;
    $data["visit_date"] = $visit_date;
    $data["poli_id"] = $poli_id;
    $data["l"] = $this->queue_model->get_in_queue($visit_date, $poli_id);

    $this->load->view("user/queue/in_q", $data);
  }

}
