<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    validate_login_session();

    $this->load->model("day_off_model");
  }

  public function index()
  {
    $data["jadwal"] = $this->day_off_model->get_jadwal();
    $this->load->view("user/dashboard/index", $data);
  }
}
