<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bpjs extends CI_Controller{
  var $bpjs_img = "";
  var $ktp_img = "";
  var $kk_img = "";
  var $sr_img = "";

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    $this->load->model("bpjs_model");
  }

  public function index()
  {
    $data_bpjs = $this->bpjs_model->data();
    $data["data_bpjs"] = $data_bpjs;

    $this->load->view("user/bpjs/index", $data);
  }

  public function add()
  {
    $this->load->view("user/bpjs/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->bpjs_model->data($id);
    $this->load->view("user/bpjs/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $bpjs_number = $this->input->post("bpjs_number");
    $name = $this->input->post("name");
    $birthdate = $this->input->post("birthdate");

    $u = $this->_do_upload("bpjs_img");
    if(!$u)
    {
      $response["status"] = false;
      $response["message"] = "Form tidak lengkap!";
    }
    $u = $this->_do_upload("ktp_img");
    if(!$u)
    {
      $response["status"] = false;
      $response["message"] = "Form tidak lengkap!";
    }
    $u = $this->_do_upload("kk_img");
    if(!$u)
    {
      $response["status"] = false;
      $response["message"] = "Form tidak lengkap!";
    }
    $u = $this->_do_upload("sr_img");
    if(!$u)
    {
      $response["status"] = false;
      $response["message"] = "Form tidak lengkap!";
    }

    $bpjs_img = $this->bpjs_img;
    $ktp_img = $this->ktp_img;
    $kk_img = $this->kk_img;
    $sr_img = $this->sr_img;

    $sr_valid_date = $this->input->post("sr_valid_date");

    $status = "active";

    $user_id = get_session("auth_id");

    if(
      !empty($bpjs_number)
      && !empty($name)
      )
      {
        $data["name"] = $name;
        $data["bpjs_number"] = $bpjs_number;
        $data["status"] = $status;
        $data["user_id"] = $user_id;
        $data["birthdate"] = $birthdate;
        $data["sr_valid_date"] = $sr_valid_date;
        $data["date_modified"] = date("Y-m-d H:i:s");

        $data["verification_status"] = "new";

        if(!empty($bpjs_img)) $data["bpjs_img"] = $bpjs_img;
        if(!empty($ktp_img)) $data["ktp_img"] = $ktp_img;
        if(!empty($kk_img)) $data["kk_img"] = $kk_img;
        if(!empty($sr_img)) $data["sr_img"] = $sr_img;

        $save = $this->bpjs_model->save($data, $id);
        if($save)
        {
          $response["status"] = true;
          $response["message"] = "Saved!";
        }
      }
      else
      {
        $response["status"] = false;
        $response["message"] = "Form tidak lengkap!";
      }

      json_file($response);
    }

    public function delete()
    {
      $id = $this->input->get("id");

      if(!empty($id))
      {
        $delete = $this->bpjs_model->delete($id);

        if($delete)
        {
          set_alert("success", "Delete Success.");
        }
        else
        {
          set_alert("danger", "Delete Failed.");
        }
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }

      redirect("user/bpjs");
    }

    public function set_status()
    {
      $id = $this->input->get("id");

      if(!empty($id))
      {
        $status = $this->input->get("status");
        $update = $this->bpjs_model->set_status($status, $id);

        if($update)
        {
          set_alert("success", ucfirst($status) . " Success.");
        }
        else
        {
          set_alert("danger", ucfirst($status) . " Failed.");
        }
      }
      else
      {
        set_alert("danger", ucfirst($status) . " Failed.");
      }

      redirect("user/bpjs");
    }

    private function _do_upload($f)
    {
      $bpjs_number = $this->input->post("bpjs_number");
      $config['upload_path']          = './files/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['overwrite']            = true;
      $config["encrypt_name"]         = true;

      $this->load->library('upload', $config);

      if ( ! $this->upload->do_upload($f))
      {
        return false;
      }
      else
      {
        switch ($f) {
          case 'bpjs_img':
          $this->bpjs_img = $this->upload->data('file_name');
          break;
          case 'ktp_img':
          $this->ktp_img = $this->upload->data('file_name');
          break;
          case 'kk_img':
          $this->kk_img = $this->upload->data('file_name');
          break;
          case 'sr_img':
          $this->sr_img = $this->upload->data('file_name');
          break;
        }

        return true;
      }
    }
  }
