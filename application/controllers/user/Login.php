<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->model("login_model");
  }

  public function index()
  {
    $this->load->view("user/login");
  }

  public function validate()
  {
    // Set Session only
    $email = $this->input->post("email");
    $password = $this->input->post("password");

    // Check Login
    // ->check_login($email, $password);
    $check_login = $this->login_model->check_login($email, $password);

    if($check_login)
    {
      redirect("user/dashboard");
    }
    else
    {
      set_alert("danger", "Auth failed, email/email or password wrong.");
      redirect("user/login?alert=error_auth_failed");
    }
  }
}
