<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function login_validation()
  {
    $this->load->model("login_model");

    $check = $this->login_model->validate_login_session();

    if($check == false)
    {
      $response["status"] = false;
      $response["message"] = "Loggin session failed!";
    }
    else
    {
      $response["status"] = true;
      $response["message"] = "Logged In.";
    }

    json_file($response);
  }

}
