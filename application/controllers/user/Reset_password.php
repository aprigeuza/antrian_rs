<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_password extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
  }

  function index()
  {
    $this->load->view("user/reset_password");
  }

  function request_otp()
  {
    $this->load->library('email');

    $email = $this->input->post("email");

    if($email == "")
    {
      $response["status"] = false;
      $response["message"] = "Email tidak boleh kosong!";
      json_file($response);
    }

    // Find Email in Database
    $email_exists = $this->user_model->user_exists($email, "user");

    if($email_exists == false)
    {
      $response["status"] = false;
      $response["message"] = "Email/Akun tidak terdaftar!";
      json_file($response);
    }

    $this->session->set_userdata("otp_email", $email);

    // Create OTP Code
    $otp_code = rand(1000, 9999);

    $data["email"] = $email;
    $data["otp_code"] = $otp_code;
    $data["request_date"] = date("Y-m-d H:i:s");
    $data["expire_date"] = date('Y-m-d H:i:s', strtotime('+1 days', strtotime($data["request_date"])));
    $this->db->replace("user_otp", $data);

    // Send Email
    $config['mailpath'] = "\"C:\xampp\sendmail\sendmail.exe\" -t";
    // $config['mailpath'] = '/usr/sbin/sendmail';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = TRUE;
    $config['protocol'] = 'smtp';
    $config['smtp_crypto'] = 'ssl';
    $config['smtp_host'] = 'smtp.gmail.com';
    $config['smtp_port'] = 465;
    $config['smtp_user'] = "antrianonlinersudbalarajatemp@gmail.com";
    $config['smtp_pass'] = "2020indonesiamenang";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";

    $this->email->initialize($config);

    $this->email->from('antrianonlinersudbalarajatemp@gmail.com', 'RSUD Balaraja');
    $this->email->to($email);
    $this->email->subject('Request OTP | Antrian Online RSUD Balara');
    $this->email->message('<h3>Kode OTP Anda : <b>'.$otp_code.'</b></h3>');
    $this->email->send();

    // You need to pass FALSE while sending in order for the email data
    // to not be cleared - if that happens, print_debugger() would have
    // nothing to output.
    // $this->email->send(FALSE);

    // Will only print the email headers, excluding the message subject and body
    // echo $this->email->print_debugger(array('headers'));

    $response["status"] = true;
    $response["message"] = "Berhasil, silahkan cek email Anda. Berikutnya, masukkan Kode OTP yang kami kirimkan.";
    json_file($response);
  }

  function verify_otp()
  {
    $email = $this->session->userdata("otp_email");
    $otp_code = $this->input->post("otp_code");

    if($otp_code == "" or strlen($otp_code) < 4)
    {
      $response["status"] = false;
      $response["message"] = "Pastikan Anda telah mengisi kode OTP dengan benar!";
      json_file($response);
    }


    $this->db->from("user_otp");
    $this->db->where("email", $email);
    $this->db->where("otp_code", $otp_code);
    $this->db->where("expire_date >", date("Y-m-d H:i:s"));
    $v = $this->db->get();
    if($v->num_rows() == 1)
    {
      $this->session->set_userdata("otp_code", $otp_code);

      $response["status"] = true;
      $response["message"] = "Berhasil, silahkan buat password baru Anda.";
      json_file($response);
    }

    $response["status"] = false;
    $response["message"] = "Kode OTP tidak valid!";
    json_file($response);
  }

  function set_new_password()
  {
    $password = $this->input->post("password");

    if($password == "" or strlen($password) < 6)
    {
      $response["status"] = false;
      $response["message"] = "Pastikan Anda telah mengisi kode OTP dengan benar!";
      json_file($response);
    }

    $email = $this->session->userdata("otp_email");
    $otp_code = $this->session->userdata("otp_code");
    $this->db->from("user_otp");
    $this->db->where("email", $email);
    $this->db->where("otp_code", $otp_code);
    $this->db->where("expire_date >", date("Y-m-d H:i:s"));
    $v = $this->db->get();
    if($v->num_rows() == 1)
    {
      $data["password"] = $this->encryption->encrypt($password);
      $where["email"] = $this->session->userdata("otp_email");
      $this->db->update("user", $data, $where);

      $this->session->unset_userdata("otp_email");
      $this->session->unset_userdata("otp_code");

      $response["status"] = true;
      $response["message"] = "Berhasil, silahkan buat password baru Anda.";
      json_file($response);
    }

    $response["status"] = false;
    $response["message"] = "Kode OTP tidak valid!";
    json_file($response);
  }

}
