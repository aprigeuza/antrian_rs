<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_manager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

    $this->load->model("login_model");

    $logged_in = $this->login_model->validate_login_session();

    if(!$logged_in) exit();
	}

	public function connect()
	{
		$opts = array(
			'debug' => true,
			'roots' => array(
				// Items volume
				array(
					'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
					'path'          => $this->config->item("front_base_path") . "files",          // path to files (REQUIRED)
					'URL'           => $this->config->item("front_base_url") . "files", 		    // URL to files (REQUIRED)
					'trashHash'     => 't1_Lw',                     // elFinder's hash of trash folder
					'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
					'uploadAllow'   => array('image', 'text/plain'),// Mimetype `image` and `text/plain` allowed to upload
					'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
					'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
				),
				// Trash volume
				array(
					'id'            => '1',
					'driver'        => 'Trash',
					'path'          => $this->config->item("front_base_path") . "files/.trash",
					'tmbURL'        => $this->config->item("front_base_url") . 'files/.trash/.tmb',
					'uploadDeny'    => array('all'),                // Recomend the same settings as the original volume that uses the trash
					'uploadAllow'   => array('image', 'text/plain'),// Same as above
					'uploadOrder'   => array('deny', 'allow'),      // Same as above
					'accessControl' => 'access',                    // Same as above
				)
			)
		);
		$this->load->library('elfinder_lib', $opts);
	}

	public function index()
	{
		$data["element_id"] = $this->input->get("element_id");
		$this->load->view("file_manager/popup", $data);
	}

  public function modal()
  {
    $data["element_id"] = $this->input->get("element_id");
    $this->load->view("file_manager/popup", $data);
  }

	public function ckeditor()
	{
		$data["CKEditorFuncNum"] = $this->input->get("CKEditorFuncNum");
		$this->load->view("file_manager/ckeditor", $data);
	}
}
