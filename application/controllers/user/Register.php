<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->library("encryption");
    $this->load->model("register_model");
  }

  function index()
  {
    $this->load->view("user/register");
  }

  function save()
  {
    $response = array();

    $name = $this->input->post("name");
    $email = $this->input->post("email");
    $password = $this->input->post("password");
    $password_conf = $this->input->post("password_conf");

    if($name == "")
    {
      $response["status"] = false;
      $response["message"] = "Harap isi Nama.";
      json_file($response);
    }

    if($email == "")
    {
      $response["status"] = false;
      $response["message"] = "Harap isi Email.";
      json_file($response);
    }

    if($password == "")
    {
      $response["status"] = false;
      $response["message"] = "Harap isi Password.";
      json_file($response);
    }

    if($password_conf == "")
    {
      $response["status"] = false;
      $response["message"] = "Harap isi Konfirmasi Password.";
      json_file($response);
    }

    if($password_conf !== $password)
    {
      $response["status"] = false;
      $response["message"] = "Konfirmasi Password tidak sama.";
      json_file($response);
    }

    $email_exists = $this->register_model->email_exists($email);
    if($email_exists)
    {
      $response["status"] = false;
      $response["message"] = "Email sudah terdaftar.";
      json_file($response);
    }

    $data["name"] = $name;
    $data["username"] = $email;
    $data["email"] = $email;
    $data["level"] = "user";
    $data["password"] = $this->encryption->encrypt($password);

    $this->register_model->save($data);

    $response["status"] = true;
    $response["message"] = "Success.";
    json_file($response);
  }

}
