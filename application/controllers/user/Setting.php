<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    $this->load->model("setting_model");
  }

  public function index()
  {
    $data = array();
    $this->load->view("user/setting/index", $data);
  }

  public function save_account()
  {
    if(empty($this->input->post("name")))
    {
      set_alert("danger", "Nama Tidak Boleh Kosong.");
      redirect("user/setting");

    }
    $data["name"] = $this->input->post("name");
    $data["phone_number"] = $this->input->post("phone_number");
    $where["id"] = get_session("auth_id");
    $this->db->update("user", $data, $where);
    if($this->db->affected_rows())
    {
      $this->session->set_userdata("auth_name", $data["name"]);
      $this->session->set_userdata("auth_phone_number", $data["phone_number"]);
      set_alert("success", "Berhasil menyimpan data.");
      redirect("user/setting");
    }
    else
    {
      set_alert("danger", "Terjadi kesalahan saat menyimpan data.");
      set_alert("danger", "Terjadi kesalahan saat menyimpan data.");
      redirect("user/setting");

    }
  }

}
