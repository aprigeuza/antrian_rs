<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    session_destroy();

    $logout = $this->login_model->logout();

    if($logout)
    {
      redirect("user/login?alert=logout");
    }
    else
    {
      redirect("user/404");
    }
  }

}
