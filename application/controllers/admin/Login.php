<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view("admin/login");
  }

  public function validate()
  {
    // Set Session only
    $username = $this->input->post("username");
    $password = $this->input->post("password");

    // Check Login
    // ->check_login($username, $password);
    $check_login = $this->login_model->check_login($username, $password);

    if($check_login)
    {
      redirect("admin/dashboard");
    }
    else
    {
      set_alert("danger", "Auth failed, username/email or password wrong.");
      redirect("admin/login?alert=error_auth_failed");
    }
  }

  public function logout()
  {
    $logout = $this->login_model->logout();

    if($logout)
    {
      $this->session->sess_destroy();
      redirect("admin/login?alert=logout");
    }
    else
    {
      redirect("admin/404");
    }
  }
}
