<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    validate_login_session(); // Check Login Session

    $this->load->model("setting_model");
  }

  public function index()
  {
    $data = array();
    $this->load->view("admin/setting/index", $data);
  }

  public function save()
  {
    $fields = $this->input->post();

    foreach($fields as $name => $value)
    {
      $d["name"] = $name;
      $d["value"] = $value;
      $this->setting_model->replace($d);
    }
    $response["status"] = true;
    $response["message"] = "Saved!";

    json_file($response);
  }

}
