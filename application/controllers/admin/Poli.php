<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Poli extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("poli_model");
  }

  public function index()
  {
    $data_poli = $this->poli_model->data();
    $data["data_poli"] = $data_poli;

    $this->load->view("admin/poli/index", $data);
  }

  public function add()
  {
    $this->load->view("admin/poli/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->poli_model->data($id);
    $this->load->view("admin/poli/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $name = $this->input->post("name");
    $prefix_code = $this->input->post("prefix_code");
    $close_time = $this->input->post("close_time");

    $status = $this->input->post("status");

    $max_visitor = unmask_money($this->input->post("max_visitor"));

    $user_id = get_session("auth_id");

    if(!empty($name))
    {
      $data["name"] = $name;
      $data["prefix_code"] = $prefix_code;

      $data["close_time"] = date("H:i", strtotime($close_time));
      $data["status"] = $status;

      $data["max_visitor"] = $max_visitor;

      $data["date_modified"] = date("Y-m-d H:i:s");

      $save = $this->poli_model->save($data, $id);

      if($save)
      {
        $response["status"] = true;
        $response["message"] = "Saved!";
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "name can't be empty!";
    }

    json_file($response);
  }

  public function delete()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $delete = $this->poli_model->delete($id);

      if($delete)
      {
        set_alert("success", "Delete Success.");
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }
    }
    else
    {
      set_alert("danger", "Delete Failed.");
    }

    redirect("admin/poli");
  }

  public function set_status()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $status = $this->input->get("status");
      $update = $this->poli_model->set_status($status, $id);

      if($update)
      {
        set_alert("success", ucfirst($status) . " Success.");
      }
      else
      {
        set_alert("danger", ucfirst($status) . " Failed.");
      }
    }
    else
    {
      set_alert("danger", ucfirst($status) . " Failed.");
    }

    redirect("admin/poli");
  }

}
