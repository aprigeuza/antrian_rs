<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("queue_model");
  }

  public function index()
  {
    $data["total_antrian_hari_ini"] = $this->queue_model->r_total_today();
    $data["total_antrian_minggu_ini"] = $this->queue_model->r_total_week();
    $data["total_antrian_bulan_ini"] = $this->queue_model->r_total_month();
    $this->load->view("admin/dashboard/index", $data);
  }
}
