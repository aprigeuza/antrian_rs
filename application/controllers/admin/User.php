<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $this->load->model("user_model");
  }

  function index()
  {
    if(get_session("auth_level") != "sa") redirect(404);

    $data = array();

    $data["data_user"] = $this->user_model->data();
    $this->load->view("admin/user/index", $data);
  }

  public function add()
  {
    if(get_session("auth_level") != "sa") redirect(404);

    $data = array();

    $this->load->view("admin/user/add", $data);
  }

  public function edit()
  {
    if(get_session("auth_level") != "sa") redirect(404);
    $data = array();

    $id = $this->input->get("id");

    $edit = $this->user_model->get_data($id);

    if($edit == false) redirect(404);

    $data["edit"] = $edit;
    $this->load->view("admin/user/edit", $data);
  }

  public function save()
  {
    if(get_session("auth_level") != "sa") redirect(404);

    $id = $this->input->post("id");
    $name = $this->input->post("name");
    $email = $this->input->post("email");
    $phone_number = $this->input->post("phone_number");
    $username = $this->input->post("username");
    $password = $this->input->post("password");
    $password2 = $this->input->post("password2");
    $level = $this->input->post("level");
    $obsolete = $this->input->post("obsolete");

    // New Or Update

    if($password == $password2)
    {
      if($id)
      {
        // $data["username"] = $username;
        $data["name"] = $name;
        $data["email"] = $email;
        $data["phone_number"] = $phone_number;
        $data["password"] = $this->encryption->encrypt($password);
        $data["level"] = $level;
        $data["obsolete"] = $obsolete;
        $this->user_model->update($data, $id);
      }
      else
      {
        $data["name"] = $name;
        $data["email"] = $email;
        $data["phone_number"] = $phone_number;
        $data["username"] = $username;
        $data["password"] = $this->encryption->encrypt($password);
        $data["level"] = $level;
        $data["obsolete"] = $obsolete;
        $this->user_model->insert($data);
      }
      $response["status"] = true;
      $response["message"] = "Saved!";
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Password tidak sama!";
    }
    json_file($response);
  }

  public function delete()
  {
    if(get_session("auth_level") != "sa") redirect(404);

    $id = $this->input->get("id");

    $this->user_model->delete($id);

    set_alert("success", "Deleted.");
    redirect("admin/user");
  }

  public function change_password()
  {
    $old_password = $this->input->post("old_password");
    $new_password = $this->input->post("new_password");
    $new_password2 = $this->input->post("new_password2");

    if(($new_password == $new_password2) && ($this->encryption->decrypt(get_session("auth_password")) == $old_password))
    {
      $data["password"] = $this->encryption->encrypt($new_password);
      $this->user_model->update($data, get_session("auth_id"));
      set_alert("success", "Password changed.");
      redirect("admin/logout");
    }
    else
    {
      set_alert("danger", "Change Password, Failed!");
      redirect("admin/dashboard");
    }
  }

}
