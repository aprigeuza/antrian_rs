<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Queue_list extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More

    $ses_queue_list_filter_date = $this->session->userdata("ses_queue_list_filter_date");
    if($ses_queue_list_filter_date == NULL)
    {
      $this->session->set_userdata("ses_queue_list_filter_date", date("Y-m-d"));
    }

    $this->load->model("queue_model");
  }

  function index()
  {
    $this->load->view("admin/queue_list/index");
  }

  public function get_table()
  {
    $visit_date = $this->input->post("visit_date");
    $this->session->set_userdata("ses_queue_list_filter_date", $visit_date);

    $data["list"] = $this->queue_model->get_by_visit_date($visit_date);

    $this->load->view("admin/queue_list/table", $data);
  }

  public function download()
  {
    $visit_date = $this->session->userdata("ses_queue_list_filter_date");
    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=queue_list_{$visit_date}.xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);

    $data["list"] = $this->queue_model->get_by_visit_date($visit_date);

    $this->load->view("admin/queue_list/table_download", $data);
  }

}
