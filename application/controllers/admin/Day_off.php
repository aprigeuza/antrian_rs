<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Day_off extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("day_off_model");
    $this->load->model("doctor_model");
    $this->load->model("poli_model");
  }

  public function index()
  {
    $data_day_off = $this->day_off_model->data();
    $data["data_day_off"] = $data_day_off;

    $this->load->view("admin/day_off/index", $data);
  }

  public function add()
  {
    $data["doctor"] = $this->doctor_model->data();
    $this->load->view("admin/day_off/add", $data);
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->day_off_model->data($id);
    $data["doctor"] = $this->doctor_model->data();
    $this->load->view("admin/day_off/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $doctor_id = $this->input->post("doctor_id");
    $_doctor_id = explode(",", $doctor_id);
    $doctor_id = $_doctor_id[1];
    $poli_id = $_doctor_id[0];
    $day_off_date = $this->input->post("day_off_date");
    $note = $this->input->post("note");

    if(!empty($doctor_id))
    {
      $data["poli_id"] = $poli_id;
      $data["doctor_id"] = $doctor_id;
      $data["day_off_date"] = $day_off_date;
      $data["note"] = $note;

      $save = $this->day_off_model->save($data, $id);

      if($save)
      {
        $response["status"] = true;
        $response["message"] = "Saved!";
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Nomor can't be empty!";
    }

    json_file($response);
  }

  public function delete()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $delete = $this->day_off_model->delete($id);

      if($delete)
      {
        set_alert("success", "Delete Success.");
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }
    }
    else
    {
      set_alert("danger", "Delete Failed.");
    }

    redirect("admin/day_off");
  }

}
