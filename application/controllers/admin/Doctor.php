<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("doctor_model");
    $this->load->model("poli_model");
  }

  public function index()
  {
    $data_doctor = $this->doctor_model->data();
    $data["data_doctor"] = $data_doctor;

    $this->load->view("admin/doctor/index", $data);
  }

  public function add()
  {
    $data["poli"] = $this->poli_model->data();
    $this->load->view("admin/doctor/add", $data);
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->doctor_model->data($id);
    $data["poli"] = $this->poli_model->data();
    $this->load->view("admin/doctor/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $poli_id = $this->input->post("poli_id");
    $name = $this->input->post("name");
    $phone_number = $this->input->post("phone_number");
    $status = $this->input->post("status");

    if(!empty($name))
    {
      $data["poli_id"] = $poli_id;
      $data["name"] = $name;
      $data["phone_number"] = $phone_number;
      $data["status"] = $status;

      $data["date_modified"] = date("Y-m-d H:i:s");

      $save = $this->doctor_model->save($data, $id);

      if($save)
      {
        $response["status"] = true;
        $response["message"] = "Saved!";
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Nomor can't be empty!";
    }

    json_file($response);
  }

  public function delete()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $delete = $this->doctor_model->delete($id);

      if($delete)
      {
        set_alert("success", "Delete Success.");
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }
    }
    else
    {
      set_alert("danger", "Delete Failed.");
    }

    redirect("admin/doctor");
  }

  public function set_status()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $status = $this->input->get("status");
      $update = $this->doctor_model->set_status($status, $id);

      if($update)
      {
        set_alert("success", ucfirst($status) . " Success.");
      }
      else
      {
        set_alert("danger", ucfirst($status) . " Failed.");
      }
    }
    else
    {
      set_alert("danger", ucfirst($status) . " Failed.");
    }

    redirect("admin/doctor");
  }

}
