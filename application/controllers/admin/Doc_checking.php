<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doc_checking extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("bpjs_model");
    $this->load->model("poli_model");
  }

  public function index()
  {
    $data_all = $this->bpjs_model->get_all();
    $data_outstanding = $this->bpjs_model->get_outstanding();
    $data["data_all"] = $data_all;
    $data["data_outstanding"] = $data_outstanding;

    $this->load->view("admin/doc_checking/index", $data);
  }

  public function detail()
  {
    $id = $this->input->post("id");

    $data["detail"] = $this->bpjs_model->get_detail($id);

    $this->load->view("admin/doc_checking/detail", $data);
  }

  public function verification()
  {
    $id = $this->input->get("id");

    $this->bpjs_model->verification($id);

    redirect("admin/doc_checking");
  }

  public function reject()
  {
    $id = $this->input->get("id");
    $verification_info = $this->input->get("verification_info");

    $this->bpjs_model->reject($id,$verification_info);

    redirect("admin/doc_checking");
  }

}
