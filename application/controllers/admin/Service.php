<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("service_model");
  }

  public function index()
  {
    $data_service = $this->service_model->data();
    $data["data_service"] = $data_service;

    $this->load->view("admin/service/index", $data);
  }

  public function add()
  {
    $this->load->view("admin/service/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->service_model->data($id);
    $this->load->view("admin/service/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $name = $this->input->post("name");
    $prefix_code = $this->input->post("prefix_code");

    $required_contract_number = $this->input->post("required_contract_number");
    $status = $this->input->post("status");

    $max_visitor_monday = unmask_money($this->input->post("max_visitor_monday"));
    $max_visitor_tuesday = unmask_money($this->input->post("max_visitor_tuesday"));
    $max_visitor_wednesday = unmask_money($this->input->post("max_visitor_wednesday"));
    $max_visitor_thursday = unmask_money($this->input->post("max_visitor_thursday"));
    $max_visitor_friday = unmask_money($this->input->post("max_visitor_friday"));
    $max_visitor_saturday = unmask_money($this->input->post("max_visitor_saturday"));

    $user_id = get_session("auth_id");

    if(!empty($name))
    {
      $data["name"] = $name;
      $data["prefix_code"] = $prefix_code;

      $data["required_contract_number"] = $required_contract_number;
      $data["status"] = $status;

      $data["max_visitor_monday"] = $max_visitor_monday;
      $data["max_visitor_tuesday"] = $max_visitor_tuesday;
      $data["max_visitor_wednesday"] = $max_visitor_wednesday;
      $data["max_visitor_thursday"] = $max_visitor_thursday;
      $data["max_visitor_friday"] = $max_visitor_friday;
      $data["max_visitor_saturday"] = $max_visitor_saturday;

      $data["date_modified"] = date("Y-m-d H:i:s");

      $save = $this->service_model->save($data, $id);

      if($save)
      {
        $response["status"] = true;
        $response["message"] = "Saved!";
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "name can't be empty!";
    }

    json_file($response);
  }

  public function delete()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $delete = $this->service_model->delete($id);

      if($delete)
      {
        set_alert("success", "Delete Success.");
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }
    }
    else
    {
      set_alert("danger", "Delete Failed.");
    }

    redirect("admin/service");
  }

  public function set_status()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $status = $this->input->get("status");
      $update = $this->service_model->set_status($status, $id);

      if($update)
      {
        set_alert("success", ucfirst($status) . " Success.");
      }
      else
      {
        set_alert("danger", ucfirst($status) . " Failed.");
      }
    }
    else
    {
      set_alert("danger", ucfirst($status) . " Failed.");
    }

    redirect("admin/service");
  }

}
