<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    $this->load->model("queue_model");
  }

  function index()
  {
    $data = array();
    $this->load->view("admin/report/index", $data);
  }

  function get_result()
  {
    $date_start = $this->input->post("date_start");
    $date_end = $this->input->post("date_end");
    $status = $this->input->post("status");

    $this->session->set_userdata("date_start", $date_start);
    $this->session->set_userdata("date_end", $date_end);
    $this->session->set_userdata("status", $status);

    $y = date("Y", strtotime($date_start));

    $data["result_data"] = $this->queue_model->get_by_period($date_start, $date_end, $status);
    $data["chart_data_finish"] = $this->queue_model->get_for_line_chart("finish", $y);
    $data["chart_data_cancel"] = $this->queue_model->get_for_line_chart("cancel", $y);

    $data["y"] = $y;
    $data["date_start"] = $date_start;
    $data["date_end"] = $date_end;
    $data["status"] = $status;

    $this->load->view("admin/report/result", $data);

  }

  function download_excell()
  {
    header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
    header("Content-Disposition: attachment; filename=report.xls");  //File name extension was wrong
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);

    $date_start = $this->session->userdata("date_start");
    $date_end = $this->session->userdata("date_end");
    $status = $this->session->userdata("status");

    $data["result_data"] = $this->queue_model->get_by_period($date_start, $date_end, $status);
    $data["date_start"] = $date_start;
    $data["date_end"] = $date_end;
    $data["status"] = $status;

    $this->load->view("admin/report/result_excell", $data);
  }

}
