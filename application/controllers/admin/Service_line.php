<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_line extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    validate_login_session(); // Check Login Session

    $this->load->model("service_line_model");
  }

  public function index()
  {
    $data_service_line = $this->service_line_model->data();
    $data["data_service_line"] = $data_service_line;

    $this->load->view("admin/service_line/index", $data);
  }

  public function add()
  {
    $this->load->view("admin/service_line/add");
  }

  public function edit()
  {
    $id = $this->input->get("id");
    $data["edit"] = $this->service_line_model->data($id);
    $this->load->view("admin/service_line/edit", $data);
  }

  public function save()
  {
    $response = array();

    $id = $this->input->post("id");
    $line_number = unmask_money($this->input->post("line_number"));
    $note = $this->input->post("note");
    $status = $this->input->post("status");

    if(!empty($line_number))
    {
      $data["line_number"] = $line_number;
      $data["note"] = $note;
      $data["status"] = $status;

      $data["date_modified"] = date("Y-m-d H:i:s");

      $save = $this->service_line_model->save($data, $id);

      if($save)
      {
        $response["status"] = true;
        $response["message"] = "Saved!";
      }
    }
    else
    {
      $response["status"] = false;
      $response["message"] = "Nomor can't be empty!";
    }

    json_file($response);
  }

  public function delete()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $delete = $this->service_line_model->delete($id);

      if($delete)
      {
        set_alert("success", "Delete Success.");
      }
      else
      {
        set_alert("danger", "Delete Failed.");
      }
    }
    else
    {
      set_alert("danger", "Delete Failed.");
    }

    redirect("admin/service_line");
  }

  public function set_status()
  {
    $id = $this->input->get("id");

    if(!empty($id))
    {
      $status = $this->input->get("status");
      $update = $this->service_line_model->set_status($status, $id);

      if($update)
      {
        set_alert("success", ucfirst($status) . " Success.");
      }
      else
      {
        set_alert("danger", ucfirst($status) . " Failed.");
      }
    }
    else
    {
      set_alert("danger", ucfirst($status) . " Failed.");
    }

    redirect("admin/service_line");
  }

}
