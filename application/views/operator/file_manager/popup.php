<!DOCTYPE html>
<html>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('elfinder/jquery-ui/css/base/jquery-ui.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('elfinder/css/theme.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('elfinder/css/elfinder.min.css'); ?>" />
	<script type="text/javascript" src="<?php echo base_url('elfinder/jquery-1.7.2.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('elfinder/jquery-ui/js/jquery-ui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('elfinder/js/elfinder.min.js'); ?>"></script>

	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#elfinder-tag').elfinder({
				url: '<?php echo base_url('file_manager/connect'); ?>',
				resizable: false,
				commandsOptions:{
					getfile:{
						onlyURL: false,
						folders: false,
						multiple: false,
						oncomplete: "destroy"
					}
				},
				getFileCallback: function (file) {
          window.parent.document.getElementById("<?php echo $element_id; ?>").value = file.url;
          window.parent.$( "#JQueryDialog").dialog('close');
				}
			}).elfinder('instance');
		});
	</script>
</head>
<body style="margin:0;padding:0">
	<div id="container">
		<div id="body">
			<div id="elfinder-tag"></div>
		</div>
	</div>
</html>
