<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Queue List</title>
  <?php echo assets_top(); ?>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url(); ?>logo.png">
  
  <style media="screen">
  .inf div.row{
    margin-top:15px;
    margin-bottom:15px;
  }
  .inf span {
    font-size: 16px;
  }
  .border-bottom{
    border-bottom: 1px solid #ddd;
  }
  </style>


  <style>
  .active_queue, .total{
    font-size: 24px;
    font-weight: bold;
    text-align: center;
  }

  .queue_list{
    display:block;
    width: auto;
    padding-left: 15px;
    padding-right: 15px;
  }

  .queue_list .item{
    display: block;
    width: 100%;

    font-size: 16px;
    text-align: center;
    }

  .panel-title{
    font-size: 20px;
    font-weight: bold;
  }

  .page-header{
    font-weight: bold;
    font-size: 36px !important;
    border-bottom: 1px solid #ddd;
  }

  #active_queue{
    font-size: 48px;
    font-weight: bold;
    text-align: center;

    margin-top: 60px;
    margin-bottom: 60px;
  }

  .bg-hijau{
    background-color: rgb(0, 172, 17);
    color: #fff;
  }

  .bg-yellow{
    background-color: rgb(190, 194, 0);
    color: #fff;
  }

  #total{
    font-size: 48px;
    font-weight: bold;
    text-align: center;

    margin-top: 60px;
    margin-bottom: 60px;
  }

  .qc{
    min-height: 500px;
    overflow-y: auto;
  }

  #queue_list .item{
    border: 1px solid rgb(24, 69, 157);
    margin-top: 3px;
    margin-bottom: 3px;
    padding-top: 10px;
    padding-bottom: 10px;
    padding-left: 15px;
    padding-right: 15px;

    background-color: rgb(24, 69, 157);
    color:#fff;

    font-size: 24px;
    font-weight: bold;
    text-align: center;
  }

  #queue_list .item.active{
    background-color: rgb(0, 172, 17);
    color: #fff;
    border: 1px solid rgb(0, 172, 17);

    padding-top: 25px;
    padding-bottom: 25px;
    padding-left: 15px;
    padding-right: 15px;
  }
  </style>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> layout-top-nav">
  <div class="wrapper">
    <div class="content-wrapper">
      <div class="container-fluid">
        <section class="" style="margin-top:15px;">
          <h1 class="page-header text-center">ANTRIAN <br>POLI <?php echo strtoupper( $poli->name); ?></h1>
          <?php echo show_alert(); ?>
          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">List Antrian</h3>
                </div>
                <div class="panel-body qc">

                  <div class="queue-list" id="queue_list">
                    <div class="item active">---</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-success">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Antrian Sekarang</h3>
                </div>
                <div class="panel-body bg-hijau">
                  <div class="active-queue" id="active_queue">---</div>
                </div>
              </div>

                <div class="panel panel-warning">
                  <div class="panel-heading">
                    <h3 class="panel-title text-center">Total Antrian</h3>
                  </div>
                  <div class="panel-body bg-yellow">
                    <div class="total" id="total">---</div>
                </div>
            </div>

            <div class="col-md-12">
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
  <?php echo assets_bottom(); ?>
  <script>
  function getResult(){
    $.ajax({
      url: '<?php echo base_url("operator/display/get_result"); ?>',
      type:'GET',
      dataType: 'json'
    })
    .done(function(respone){
      if (respone.active_queue==false) {
        $("#active_queue").html('---');
      } else {
        $("#active_queue").html(respone.active_queue.queue_code);
      }
      $("#total").html(respone.total_rows);

      if (respone.queue_list==false) {
        $("#queue_list").html('Belum ada Antrian');
      } else {
        var html= '';
        $.each(respone.queue_list, function(index, item){
          console.log(item);
          if(respone.active_queue == item.queue_code){
            html += '<div class="item active">' +item.queue_code+'</div>';
          }else{
            html += '<div class="item">' +item.queue_code+'</div>';
          }
        });
        $("#queue_list").html(html);
      }
      console.log("succes");
    })
    .fail(function(){
      console.log("error");
    })
    .always(function(){
      console.log("complete");

      setTimeout(function(){
        getResult();
      }, 5000)
    });
  }

  $(document).ready(function() {
    getResult();
  });
  </script>
</body>
</html>
