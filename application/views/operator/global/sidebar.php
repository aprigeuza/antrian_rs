<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li <?php if($this->router->fetch_class() == "dashboard") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/dashboard"); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "service") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/service"); ?>">
          <i class="fa fa-pencil"></i>
          <span>Layanan</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "service_line") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/service_line"); ?>">
          <i class="fa fa-cube"></i>
          <span>Meja</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "queue_list") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/queue_list"); ?>">
          <i class="fa fa-list"></i>
          <span>Antrian</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "report") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/report"); ?>">
          <i class="fa fa-pie-chart"></i>
          <span>Laporan</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "setting") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/setting"); ?>">
          <i class="fa fa-gears"></i>
          <span>Pengaturan</span>
        </a>
      </li>
      <?php if (get_session("auth_level") == "sa"): ?>
      <li <?php if($this->router->fetch_class() == "user") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/user"); ?>">
          <i class="fa fa-users"></i>
          <span>User</span>
        </a>
      </li>
      <?php endif; ?>
      <li <?php if($this->router->fetch_class() == "logout") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("operator/logout"); ?>">
          <i class="fa fa-sign-out"></i>
          <span>Logout</span>
        </a>
      </li>
    </ul>
  </section>
</aside>
