<!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <?php echo assets_top(); ?>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url(); ?>logo.png">
  
  <style media="screen">
    .inf div.row{
      margin-top:15px;
      margin-bottom:15px;
    }
    .inf span {
      font-size: 16px;
    }
    .border-bottom{
        border-bottom: 1px solid #ddd;
    }
  </style>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> layout-top-nav">
<div class="wrapper">
  <?php echo page_header(); ?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <section class="content">
        <?php echo show_alert(); ?>
        <div class="row">
          <div class="col-md-2">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Operator</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body text-center">
                <span style="font-size:32px;line-height:80px;"><b><?php echo get_session("auth_name"); ?></b></span> <br>
              </div>
              <!-- /.box-body -->
            </div>
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-home"></i> Poli</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body text-center">
                <span style="font-size:32px;"><b><?php echo $poli->name; ?></b></span>
              </div>
              <!-- /.box-body -->
            </div>
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-calendar"></i> Tanggal</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body text-center">
                <span style="font-size:32px;line-height:80px;"><b><?php echo date("d/M/Y"); ?></b></span>
              </div>
              <!-- /.box-body -->
            </div>
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="box-header with-border"></i> Display Antrian</h3>
            </div>
            <div class="from-group">
              <div class="box-header with-border">
                <a href="<?php echo set_url("operator","display","index"); ?>" target="_blank" class="btn btn-block btn-lg btn-primary" style="padding-top:30px;padding-bottom:30px;"> Tampilkan </a>
              </div>
        </div>
        </div>

          </div>
          <div class="col-md-4">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list"></i> List Antrian</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-bordered" id="table1">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th class="col-md-8">Nama</th>
                            <th class="col-md-2">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>

          <div class="col-md-6">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"> <i class="fa fa-list"></i> Antrian</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4 col-xs-4">
                    <button onclick="qPrev()" type="button" class="btn btn-warning btn-block" style="line-height:80px;font-weight:bold">Sebelumnya</button>
                  </div>
                  <div class="col-md-4 col-xs-4 text-center">
                    <span style="font-size:12px;">~ Sekarang ~</span> <br>
                    <span style="font-weight:bold; font-size:60px;" id="current_number">{null}</span>
                  </div>
                  <div class="col-md-4 col-xs-4">
                    <button onclick="qNext()" type="button" class="btn btn-success btn-block" style="line-height:80px;font-weight:bold">Berikutnya</button>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>


            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Informasi Pengunjung</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body inf">
                <div class="row">
                  <div class="col-md-6 text-left">
                    <span>Nama</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <span id="inf_name">{null}</span>
                  </div>
                </div>
                <div class="border-bottom">

                </div>
                <div class="row">
                  <div class="col-md-6 text-left">
                    <span>No. BPJS</span>
                  </div>
                  <div class="col-md-6 text-right">
                    <span id="inf_bpjs">{null}</span>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
  <?php echo page_footer(); ?>
</div>
<?php echo assets_bottom(); ?>

<script>
var table1 = null;
  $(function(){
    table1 = $("#table1").DataTable({
      "ajax": '<?php echo set_url("operator", "queue_list", "get_list"); ?>',
      "columns": [
          { data: 'queue_code' },
          { data: 'name' },
          { data: 'status' },
      ]
    });

    setInterval( function () {
        table1.ajax.reload();
    }, 30000 );
  });

  function qPrev(){
    $.ajax({
      url: '<?php echo set_url("operator", "queue_list", "qprev"); ?>',
      type: 'GET',
      dataType: 'JSON',
      success: function(response){
        getInf(response.id);

        if(response.status==false){
          alertify.error("Data Not Found!");
          return;
        }
      }
    })
    .done(function() {
      // console.log("success");
    })
    .fail(function() {
      // console.log("error");
    })
    .always(function() {
      // console.log("complete");
      table1.ajax.reload();
    });
  }

  function qNext(){
    $.ajax({
      url: '<?php echo set_url("operator", "queue_list", "qnext"); ?>',
      type: 'GET',
      dataType: 'JSON',
      success: function(response){
        getInf(response.id);
        if(response.status==false){
          alertify.error("Data Not Found!");
          return;
        }
      }
    })
    .done(function() {
      // console.log("success");
    })
    .fail(function() {
      // console.log("error");
    })
    .always(function() {
      // console.log("complete");
      table1.ajax.reload();
    });
  }

  function getInf(){
    $.ajax({
      url: '<?php echo set_url("operator", "queue_list", "get_inf"); ?>',
      type: 'POST',
      dataType: 'JSON',
      success: function(response){
        if(response.status == true){
          $("#current_number").html(response.data.queue_code);
          $("#inf_name").html(response.data.bpjs_name);
          $("#inf_bpjs").html(response.data.bpjs_number);
        }else{
          $("#current_number").html("{null}");
          $("#inf_name").html("{null}");
          $("#inf_bpjs").html("{null}");
        }
      }
    })
    .done(function() {
      // console.log("success");
    })
    .fail(function() {
      // console.log("error");
    })
    .always(function() {
      // console.log("complete");
    });
  }

  $(document).ready(function() {
    getInf('<?php echo $this->session->userdata("queue_number"); ?>');
  });
</script>
</body>
</html>
