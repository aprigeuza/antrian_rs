<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- Alertify -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/alertifyjs/css/alertify.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('plugins/alertifyjs/css/themes/default.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('dist/css/styles.css?t='.time()); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style media="screen">
    body{
      background-color: rgb(26, 204, 183) !important;
      background-size: cover !important;
      background-repeat: no-repeat !important;
      background-position: center !important;
      background-clip: content-box !important;
    }
    .i-title{
      margin-top:3px;margin-bottom:15px;padding-bottom: 10px;border-bottom: 1px solid #ddd;font-weight:bold;
    }

    .tab-pane{
      margin-top: 10px;
    }
  </style>
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <!-- /.login-logo -->
  <h2 class="text-center" style="font-weight:bold;color:#fff;margin-top:30px;margin-bottom:30px;">RSUD Balaraja</h2>
  <div class="login-box-body" style="border-radius:3px;">
    <h3 class="text-center i-title">E-Antrian Online<br><small>Reset Password</small></h3>

    <form id="fm1" style="display:none">
      <div class="form-group">
        <label>Masukkan Email Anda</label>
        <input class="form-control" type="text" id="email" name="email" value="" placeholder="Masukkan Email Anda">
      </div>
      <a href="<?php echo set_url("user", "login") ?>" class="btn btn-default">Batal</a>
      <div class="pull-right">
        <button type="button" class="btn btn-primary" id="btnRequestOTP">Minta kode OTP</button>
      </div>
    </form>
    <form id="fm2" style="display:none">
      <div class="form-group">
        <label>Masukkan Kode OTP</label>
        <input type="text" class="form-control" id="otp_code" name="otp_code" value="" placeholder="Masukkan Kode OTP" maxlength="4">
      </div>
      <a href="javascript:;" id="fm2BackButton" class="btn btn-default">Kembali</a>
      <div class="pull-right">
        <button type="button" id="btnVerify" class="btn btn-primary">Verifikasi</button>
      </div>
    </form>
    <form id="fm3" style="display:none">
      <div class="form-group">
        <label>Masukkan Password Baru</label>
        <input type="password" class="form-control" id="password" name="password" value="" placeholder="">
      </div>
      <div class="form-group">
        <label>Masukkan Password Lagi</label>
        <input type="password" class="form-control" id="password2" name="password2" value="" placeholder="">
      </div>
      <a href="<?php echo set_url("user", "login") ?>" class="btn btn-default">Batal</a>
      <div class="pull-right">
        <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Alertify -->
<script src="<?php echo base_url('plugins/alertifyjs/alertify.min.js'); ?>"></script>

<script src="<?php echo base_url('dist/js/app.js?time='.time()); ?>"></script>
<script>
$("#fm1").hide();
$("#fm2").hide();
$("#fm3").hide();

$("#fm1").on("submit", function(e){
  e.preventDefault();
})

$(document).ready(function() {
  $("#fm1").show();
});

$("#btnRequestOTP").on("click", function(){
  var fm = $("#fm1");
  $.ajax({
    url: '<?php echo base_url("user/reset_password/request_otp"); ?>',
    type: 'POST',
    dataType: 'JSON',
    data: fm.serializeArray(),
    beforeSend: function(){
      loadingSpinner.show();
    }
  })
  .done(function(response) {
    if(response.status == false){
      messageBox("Alert", response.message);
    }else{
      messageBox("Alert", response.message);
      $("#fm2").show();

      $("#fm1").hide();
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
});

$("#btnVerify").on("click", function(){
  var fm = $("#fm2");
  $.ajax({
    url: '<?php echo base_url("user/reset_password/verify_otp"); ?>',
    type: 'POST',
    dataType: 'JSON',
    data: fm.serializeArray(),
    beforeSend: function(){
      loadingSpinner.show();
    }
  })
  .done(function(response) {
    if(response.status == false){
      messageBox("Alert", response.message);
    }else{
      messageBox("Alert", response.message);
      $("#fm3").show();

      $("#fm2").hide();
      $("#fm1").hide();
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
});
$("#fm2BackButton").on("click", function(){
  $("#fm1").show();

  $("#fm2").hide();
  $("#fm3").hide();
});

$("#btnSave").on("click", function(){

  var fm = $("#fm3");

  if ($("#password").val() == "") {
    alertify.error("Harap isi Password.");
    $("#password").focus();
    return;
  }
  if ($("#password2").val() == "") {
    alertify.error("Harap isi Konfirmasi Password.");
    $("#password2").focus();
    return;
  }
  if ($("#password2").val() != $("#password").val()) {
    alertify.error("Konfirmasi Password tidak sama.");
    $("#password2").focus();
    return;
  }
  $.ajax({
    url: '<?php echo base_url("user/reset_password/set_new_password"); ?>',
    type: 'POST',
    dataType: 'JSON',
    data: fm.serializeArray(),
    beforeSend: function(){
      loadingSpinner.show();
    }
  })
  .done(function(response) {
    if(response.status == false){
      messageBox("Alert", response.message);
    }else{
      alertify.alert(response.message);
      messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user/login"); ?>') });
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
})
</script>

</body>
</html>
