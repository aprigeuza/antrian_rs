<!DOCTYPE html>
<html>
<head>
  <title>Dashboard</title>
  <?php echo assets_top(); ?>
  <!-- Favicon -->
  <link rel="icon" href="<?php echo base_url(); ?>logo.png">

</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <a href="<?php echo set_url("user", "bpjs", "add"); ?>" class="btn btn-block btn-lg btn-primary" style="padding-top:30px;padding-bottom:30px;">Tambah BPJS</a>
      <a href="<?php echo set_url("user", "queue", "register"); ?>" class="btn btn-block btn-lg btn-primary" style="padding-top:30px;padding-bottom:30px;">Daftar Antrian</a>
      <a href="<?php echo set_url("user", "queue"); ?>" class="btn btn-block btn-lg btn-primary" style="padding-top:30px;padding-bottom:30px;">List Antrian</a>

      <div class="row">
        <div class="col-xs-12">
          <h2>Jadwal Dokter</h2>

          <div class="panel-group" id="ac1">
            <?php foreach ($jadwal as $j): ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#ac1" href="#d<?php echo $j->doctor_id; ?>">
                      <?php echo $j->doctor_name; ?> (Poli. <?php echo $j->poli_name; ?>)
                    </a>
                  </h4>
                </div>
                <div id="d<?php echo $j->doctor_id; ?>" class="panel-collapse collapse in">
                  <div class="" style="padding:15px;">
                    <div class="row">
                      <div class="col-xs-12">
                        <table class="table table-bordered">
                          <tr>
                            <th>Hari</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                            <th>Note</th>
                          </tr>
                          <?php foreach ($j->jadwal as $j2): ?>
                            <tr <?php if($j2->status == "off") echo ' class="danger"'; ?>>
                              <td><?php echo date("D", strtotime($j2->date)); ?></td>
                              <td><?php echo date("d/m/Y", strtotime($j2->date)); ?></td>
                              <td class="text-center"><?php if($j2->status == "available"){ echo $j2->status . " at 08:00 WIB"; }else{ echo $j2->status; } ; ?></td>
                              <td><?php echo $j2->note; ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </table>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach; ?>
          </div>

        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>
</body>
</html>
