<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daftar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/iCheck/square/blue.css">
  <!-- Alertify -->
  <link rel="stylesheet" href="<?php echo base_url('plugins/alertifyjs/css/alertify.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('plugins/alertifyjs/css/themes/default.min.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style media="screen">
    body{
      background-color: rgb(26, 204, 183) !important;
      background-size: cover !important;
      background-repeat: no-repeat !important;
      background-position: center !important;
      background-clip: content-box !important;
    }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <h2 class="text-center" style="font-weight:bold;color:#fff;margin-top:30px;margin-bottom:30px;">RSUD Balaraja</h2>
  <div class="login-box-body" style="border-radius:3px;">
    <h3 class="text-center" style="margin-top:3px;margin-bottom:15px;padding-bottom: 10px;border-bottom: 1px solid #ddd;font-weight:bold;">Antrian Online<br><small>Login</small></h3>
    <?php
    $alert = show_alert();
    if (!empty($alert)): ?>
      <?php echo $alert; ?>
    <?php endif; ?>
    <form role="form" id="form1">
      <div class="form-group has-feedback">
        <label>Nama</label>
        <input type="text" class="form-control" placeholder="Nama" name="name" id="name" autofocus>
      </div>
      <div class="form-group has-feedback">
        <label>Email</label>
        <input type="email" class="form-control" placeholder="Email" name="email" id="email">
      </div>
      <div class="form-group has-feedback">
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" id="password">
      </div>
      <div class="form-group has-feedback">
        <label>Konfirmasi Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password_conf" id="password_conf">
      </div>
      <button type="button" class="btn btn-primary btn-block btn-flat btn-lg" onclick="saveForm();">Daftar</button>
      <br>
      <p class="text-center">Atau</p>
      <br>
      <a href="<?php echo set_url("user", "login"); ?>" class="btn btn-default btn-block btn-flat btn-lg">Login</a>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(); ?>plugins/iCheck/icheck.min.js"></script>
<!-- Alertify -->
<script src="<?php echo base_url('plugins/alertifyjs/alertify.min.js'); ?>"></script>
<script src="<?php echo base_url('plugins/alertifyjs/alertify.js'); ?>"></script>

<script>
  function saveForm(){
    var fm = $("#form1");

    if ($("#name").val() == "") {
      alertify.error("Harap isi Nama.");
      $("#name").focus();
      return;
    }
    if ($("#email").val() == "") {
      alertify.error("Harap isi Email.");
      $("#email").focus();
      return;
    }
    if ($("#password").val() == "") {
      alertify.error("Harap isi Password.");
      $("#password").focus();
      return;
    }
    if ($("#password_conf").val() == "") {
      alertify.error("Harap isi Konfirmasi Password.");
      $("#password_conf").focus();
      return;
    }
    if ($("#password_conf").val() != $("#password").val()) {
      alertify.error("Konfirmasi Password tidak sama.");
      $("#password_conf").focus();
      return;
    }

    $.ajax({
      url: '<?php echo base_url("user/register/save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      data: fm.serializeArray(),
      success: function(result){
        if(result.status == false){
          alertify.alert(result.message);
        }else{
          window.location='<?php echo set_url("user/dashboard"); ?>'
        }
      }
    });
  }
</script>
</body>
</html>
