<!DOCTYPE html>
<html>
<head>
  <title>Pendaftaran</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pendaftaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pendaftaran</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="form-group">
                  <label for="">Pilih BPJS</label>
                  <select class="form-control" name="bpjs_id" id="bpjs_id">
                    <option value="0">-- Pilih BPJS --</option>
                    <?php foreach($bpjs->result() as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name . " | " . $row->bpjs_number; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Pilih Poli</label>
                    <select class="form-control" name="poli_id" id="poli_id" onchange="getDoctor()">
                      <option value="0">-- Pilih Poli --</option>
                    <?php foreach($poli->result() as $row): ?>
                    <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">Pilih Doktor</label>
                  <select class="form-control" name="doctor_id" id="doctor_id">
                    <option value="0">-- Pilih Dokter --</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Pilih Tanggal</label>
                  <input class="form-control datepicker" type="text" name="visit_date" id="visit_date" value="<?php echo date("Y-m-d"); ?>" readonly style="background-color:#fff;cursor:pointer;">
                </div>
              </div>
              <div class="box-footer">
                <button onclick="saveForm()" type="button" name="button" class="btn btn-primary">Submit</button>
                <a href="<?php echo set_url("user/queue"); ?>" class="btn btn-default" title="Back">Kembali</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  function getDoctor(){
    $.ajax({
      url: '<?php echo set_url("user", "queue", "get_doctor"); ?>',
      type: 'POST',
      dataType: 'JSON',
      data: {poli_id: $("#poli_id").val()},
      success: function(response){
        // Clear List doctor
        $("#doctor_id").html("");

        $("#doctor_id").append('<option value="0">-- Pilih Dokter --</option>')
        $.each(response.data, function(i) {
          var item = response.data[i];
          $("#doctor_id").append('<option value="'+item.id+'">'+item.name+'</option>')
        });
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }

  function saveForm(){
    var f = $("#form1");

    $.ajax({
      url: '<?php echo set_url("user", "queue", "save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: f.serializeArray()
    })
    .done(function(response) {
      if(response.status == true){
        alertify.alert(response.message);
        messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user/queue/in"); ?>?id=' + response.id) });
      }else{
        messageBox("Information", response.message);
      }
    })
    .fail(function(response) {
      alertify.error('Error!');
    })
    .always(function() {
      loadingSpinner.hide();
    });
  }

  $(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate: "today",
    endDate: "+7d"
  });
</script>

</body>
</html>
