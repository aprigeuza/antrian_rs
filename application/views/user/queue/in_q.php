
<?php if ($l->num_rows()): ?>

  <ul class="list-group">
    <?php foreach($l->result() as $row): ?>

      <?php if($row->id == $queue_id): ?>
        <li class="list-group-item text-success active">
           <b><?php echo $row->queue_code; ?></b>
          <span class="pull-right">Nomor Anda</span>
        </li>
      <?php else: ?>
        <li class="list-group-item"><?php echo $row->queue_code; ?>
       <span class="pull-right">Menunggu</span></li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>


<?php else: ?>

  <div class="alert alert-danger">
    Not Found
  </div>
<?php endif; ?>
