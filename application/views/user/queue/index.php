<!DOCTYPE html>
<html>
<head>
  <title>Antrian</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Antrian
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Antrian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-xs-12">
          <?php if($queue_list->num_rows): ?>
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">List Antrian</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="list-group">
                <?php foreach($queue_list->result as $row): ?>
                <li class="list-group-item">
                  <h5 style="font-size:24px;"> <b><?php echo $row->queue_code; ?></b> </h5>
                  <span><?php echo $row->bpjs_name; ?></span>
                  <br>
                  <span><?php echo $row->poli_name; ?></span> | <span><?php echo $row->doctor_name; ?></span>
                  <br>
                  <span><?php echo $row->bpjs_number; ?></span> | <span><?php echo date("Y/m/d", strtotime($row->visit_date)); ?></span>

                  <?php if ($row->status == "new"): ?>
                  <span class="pull-right text-warning">Dalam Antrian</span>
                  <?php elseif($row->status == "cancel"): ?>
                  <span class="pull-right text-danger">Dibatalkan</span>
                  <?php elseif($row->status == "finish"): ?>
                  <span class="pull-right text-muted">Selesai</span>
                  <?php endif; ?>

                  <?php if ($row->status == "new" or $row->status == "active"): ?>
                  <div class="row">
                    <div class="col-xs-12" style="padding-top:5px;">
                      <a href="<?php echo set_url("user", "queue", "in") . "?id=" . $row->id; ?>" class="btn btn-warning">
                        <i class="fa fa-arrow-right"></i>  Lihat Antrian
                      </a>
                    </div>
                  </div>
                  <?php endif; ?>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <?php else: ?>
          <p>Belum ada histori Antrian. Klik tombol di bawah untuk mulai daftar antrian.</p>
          <center><a href="<?php echo set_url("user", "queue", "register"); ?>" class="btn btn-danger">Daftar Antrian Sekarang</a></center>
          <?php endif; ?>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>
</body>
</html>
