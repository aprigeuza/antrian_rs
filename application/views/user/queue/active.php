<!DOCTYPE html>
<html>
<head>
  <title>Antrian</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Antrian
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Antrian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <div class="row">

          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Antrian Sekarang</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <h2 class="text-center">-----</h2>
              </div>
            </div>
          </div>

          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">No Antrian Anda</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <h2 class="text-center">PD001</h2>
                <small class="text-center"><?php echo $q->name; ?></small>
                <hr class="line-separator">
                <a href="<?php echo set_url("user", "queue", "cancel"); ?>?id=__id" class="btn btn-danger btn-block" onclick="return confirm('Yakin akan membatalkan antrian?');">Batalkan Antrian</a>
              </div>
            </div>
          </div>

          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Dalam Antrian</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="max-height:350px;overflow-x:auto">
                <ul class="list-group">
                  <li class="list-group-item text-success">
                    <i class="fa fa-check"></i> <b>PD003</b>
                    <span class="pull-right">Nomor Anda</span>
                  </li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                  <li class="list-group-item">PD006</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

</body>
</html>
