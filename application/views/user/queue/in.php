<!DOCTYPE html>
<html>
<head>
  <title>Antrian</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Antrian

        <div class="pull-right">
          <a href="<?php echo set_url("user/queue/in"); ?>?id=<?php echo $q->id; ?>" class="btn btn-default">
            <i class="fa fa-refresh"></i> Refresh
          </a>
        </div>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Antrian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-info">
              <h4>Antrian <?php echo date("d/M/Y", strtotime($q->visit_date)); ?></h4>
              <b style="font-size:16px"><?php echo $q->poli_name; ?></b> | <b style="font-size:16px"><?php echo $q->doctor_name; ?></b>
            </div>
          </div>

          <div class="col-xs-12">
            <div class="row">
              <div class="col-xs-12">
                <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">No Antrian Anda</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <h2 class="text-center"><?php echo $q->queue_code; ?></h2>
                    <p class="text-center">
                    </p>
                    <hr class="line-separator">
                    <?php if ($q->status != "active" and $q->status != "finish"): ?>
                      <a id="btn_cancel" href="javascript:;" onclick="return cancelQueue('<?php echo $q->id; ?>');"  class="btn btn-danger btn-block">Batalkan</a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
              <div class="col-xs-12">
                <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Antrian Sekarang</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <h2 class="text-center" id="qactive">-----</h2>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-xs-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Dalam Antrian</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body" style="max-height:350px;overflow-x:auto" id="in_q">

              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>


<script>
  function getInQueue(){
    var poli_id = <?php echo $q->poli_id; ?>;
    var visit_date = '<?php echo $q->visit_date; ?>';
    var queue_id = <?php echo $q->id; ?>;
    $.ajax({
      url: '<?php echo set_url("user", "queue", "in_queue"); ?>',
      type: 'POST',
      dataType: 'html',
      data: {
        poli_id: poli_id,
        queue_id : queue_id,
        visit_date : visit_date
      },
      success: function (response){
        $("#in_q").html(response);
      }
    })
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
      setTimeout(function(){
        getInQueue();
      }, 60000);
    });
  }

  function cancelQueue(id){
    if(confirm('Yakin akan membatalkan Antrian?')){
      $.ajax({
        url: '<?php echo set_url("user", "queue", "cancel"); ?>',
        type: 'POST',
        dataType: 'json',
        data: {
          id : id,
        },
        success: function (response){
          if(response.status == true){
            alertify.alert(response.message);
            messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user/queue"); ?>') });
          }else{
            messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user/queue"); ?>') });
          }
        }
      })
      .done(function() {
      })
      .fail(function() {
      })
      .always(function() {
      });
    }
  }

  function qActive(){
    var poli_id = <?php echo $q->poli_id; ?>;
    $.ajax({
      url: '<?php echo set_url("user", "queue", "get_active"); ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        poli_id: poli_id
      },
      success: function (response){
        if(response.status){
          $("#qactive").html(response.data.queue_code);
        }else{
          $("#qactive").html("------");
        }
      }
    })
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
      setTimeout(function(){
        qActive();
      }, 30000);
    });
  }

  $(document).ready(function() {
    getInQueue();
    qActive();
  });
</script>

</body>
</html>
