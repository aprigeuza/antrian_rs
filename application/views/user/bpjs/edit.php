<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Home - List BPJS - Edit</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List BPJS
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo set_url("user/bpjs"); ?>">List BPJS</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>* No. BPJS</label>
                      <input type="text" class="form-control" name="bpjs_number" id="bpjs_number" value="<?php echo $edit->bpjs_number; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <label>* Nama</label>
                      <input type="text" class="form-control" name="name" id="name" value="<?php echo $edit->name; ?>" required data-valid-label="Nama" minlength="2">
                    </div>
                    <div class="form-group">
                      <label>* Tanggal Lahir</label>
                      <input type="text" class="form-control datepicker" name="birthdate" id="birthdate" value="<?php echo date("Y-m-d"); ?>" required>
                    </div>
                    <div class="form-group">
                      <label>* Foto / Scan Kartu BPJS</label>
                      <input type="file" class="form-control" name="bpjs_img" id="bpjs_img" value="" required data-valid-label="BPJS">
                    </div>
                    <div class="form-group">
                      <label>* Foto / Scan KTP</label>
                      <input type="file" class="form-control" name="ktp_img" id="ktp_img" value="" required data-valid-label="KTP">
                    </div>
                    <div class="form-group">
                      <label>* Foto / Scan KK</label>
                      <input type="file" class="form-control" name="kk_img" id="kk_img" value="" required data-valid-label="KK">
                    </div>
                    <div class="form-group">
                      <label>* Foto / Scan Surat Rujukan</label>
                      <input type="file" class="form-control" name="sr_img" id="sr_img" value="" required data-valid-label="Surat Rujukan">
                    </div>
                    <div class="form-group">
                      <label>* Tanggal Berlaku Surat Rujukan</label>
                      <input type="text" class="form-control datepicker" name="sr_valid_date" id="sr_valid_date" value="<?php echo date("Y-m-d"); ?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button onclick="saveForm()" type="button" name="button" class="btn btn-primary">Simpan</button>
                <a href="<?php echo set_url("user/bpjs"); ?>" class="btn btn-default" title="Back">Kembali</a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$(document).ready(function() {
  $(".datepicker").datepicker({
    format: "yyyy-mm-dd"
  });
});

function saveForm(){
  var f = $("#form1");

  if(!isValid("#bpjs_number")) return;
  if(!isValid("#name")) return;

  if(!isValid("#bpjs_img")) return;
  if(!isValid("#ktp_img")) return;
  if(!isValid("#kk_img")) return;
  if(!isValid("#sr_img")) return;

  var formData = new FormData($("#form1")[0]);

  $.ajax({
    url: '<?php echo set_url("user/bpjs", "save"); ?>',
    type: 'POST',
    dataType: 'JSON',
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function(){
      loadingSpinner.show();
    },
    data: formData
  })
  .done(function(response) {
    if(response.status == true){
      alertify.alert(response.message);
      messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user/bpjs"); ?>') });
    }else{
      messageBox("Alert", response.message);
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
}
</script>

</body>
</html>
