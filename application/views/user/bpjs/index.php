<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>List BPJS - Dashboard</title>
  <?php echo assets_top(); ?>
  <style media="screen">
    .imgb{
      display:block;
      height:250px;
      background-position: center;
      background-size:contain;
      background-repeat: no-repeat;
    }

    .btn-ngambang{
      position: fixed;
      bottom: 15px;
      right: 15px;
      border-radius: 50%;
    }
  </style>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        List BPJS
        <div class="pull-right">
          <a href="<?php echo set_url("user/bpjs"); ?>" class="btn btn-default">
            <i class="fa fa-refresh"></i> Refresh
          </a>
        </div>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">List BPJS</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-12">

          <?php if ($data_bpjs->num_rows() == 0): ?>
            <br>
            <p class="text-warning">Anda belum menambahkan nomor BPJS.</p>
          <?php endif; ?>

          <br>
          <?php
          foreach($data_bpjs->result() as $row):
            $bpjs_img = base_url("img/no-img.png");
            if ($row->bpjs_img)
            {
              $bpjs_img = base_url("files/" . $row->bpjs_img);
            }
            $ktp_img = base_url("img/no-img.png");
            if ($row->ktp_img)
            {
              $ktp_img = base_url("files/" . $row->ktp_img);
            }
            $kk_img = base_url("img/no-img.png");
            if ($row->kk_img)
            {
              $kk_img = base_url("files/" . $row->kk_img);
            }
            $sr_img = base_url("img/no-img.png");
            if ($row->sr_img)
            {
              $sr_img = base_url("files/" . $row->sr_img);
            }
          ?>
          <div class="list-group">
            <div class="list-group-item">

              <div id="myCarousel<?php echo $row->id; ?>" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel<?php echo $row->id; ?>" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel<?php echo $row->id; ?>" data-slide-to="1"></li>
                  <li data-target="#myCarousel<?php echo $row->id; ?>" data-slide-to="2"></li>
                  <li data-target="#myCarousel<?php echo $row->id; ?>" data-slide-to="3"></li>
                  <li data-target="#myCarousel<?php echo $row->id; ?>" data-slide-to="4"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                  <div class="item active">
                    <div class="imgb" style="background-image:url('<?php echo $bpjs_img; ?>');">
                    </div>
                    <div class="carousel-caption">
                      <h3>BPJS</h3>
                    </div>
                  </div>

                  <div class="item">
                    <div class="imgb" style="background-image:url('<?php echo $ktp_img; ?>');">
                    </div>
                    <div class="carousel-caption">
                      <h3>KTP</h3>
                    </div>
                  </div>

                  <div class="item">
                    <div class="imgb" style="background-image:url('<?php echo $kk_img; ?>');">
                    </div>
                    <div class="carousel-caption">
                      <h3>Kartu Keluarga</h3>
                    </div>
                  </div>

                  <div class="item">
                    <div class="imgb" style="background-image:url('<?php echo $sr_img; ?>');">
                    </div>
                    <div class="carousel-caption">
                      <h3>Surat Rujukan</h3>
                    </div>
                  </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel<?php echo $row->id; ?>" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel<?php echo $row->id; ?>" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>


              <h4>
                <?php echo $row->bpjs_number; ?>
                <div class="pull-right">
                  <?php if ($row->verification_status == "new"): ?>
                    <span class="label label-default">Pending</span>
                  <?php endif; ?>
                  <?php if ($row->verification_status == "ok"): ?>
                    <span class="label label-success">Diterima</span>
                  <?php endif; ?>
                  <?php if ($row->verification_status == "reject"): ?>
                    <span class="label label-danger">Ditolak</span>
                  <?php endif; ?>
                </div>
              </h4>
              <p class="text-warning text-right"> <i class="fa fa-info-circle"></i> <?php echo $row->verification_info ?></p>
              <p><?php echo $row->name; ?></p>
              <hr class="line-separator">
              <?php if ($row->verification_status != "ok"): ?>
              <a href="<?php echo set_url("user", "bpjs", "edit") . "?id=" . $row->id; ?>" class="btn btn-primary">Edit</a>
              <a href="<?php echo set_url("user", "bpjs", "delete") . "?id=" . $row->id; ?>" class="btn btn-danger" onclick="return confirm('Delete?');">Delete</a>
              <?php endif; ?>

            </div>
          </div>
          <?php endforeach ?>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->

<a href="<?php echo set_url("user", "bpjs", "add"); ?>" class="btn btn-primary btn-lg btn-ngambang"> <i class="fa fa-plus"></i> </a>
<?php echo assets_bottom(); ?>

<script>
$('.carousel').carousel({
  interval: false,
});

$(document).ready(function() {
  $(".carousel").swipe({

    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

      if (direction == 'left') $(this).carousel('next');
      if (direction == 'right') $(this).carousel('prev');

    },
    allowPageScroll:"vertical"

  });
});
</script>

</body>
</html>
