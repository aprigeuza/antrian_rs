<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Admin - User - Profile</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Admin</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li><a href="<?php echo set_url("user"); ?>">User</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <div class="row">
          <div class="col-md-12">
            <button onclick="saveForm()" type="button" name="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
            <a href="<?php echo set_url("user"); ?>" class="btn btn-default btn-sm" title="Back"><i class="fa fa-reply"></i></a>
          </div>
        </div>
        <input type="hidden" name="id" value="0">
        <div class="row" style="margin-top:5px;margin-bottom:5px">
          <div class="col-md-6">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Profile User</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="form-group">
                  <label>* Level</label>
                  <select class="form-control" name="level" id="level">
                    <option value="sa" <?php if($edit->level == "sa") echo " selected"; ?>>Sistem Administrator</option>
                    <option value="us" <?php if($edit->level == "us") echo " selected"; ?>>Standard User</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>* Username</label>
                  <input type="text" class="form-control" name="username" id="username" value="<?php echo $edit->username; ?>" required data-valid-label="Username" minlength="2">
                </div>
                <div class="form-group">
                  <label>* Password</label>
                  <input type="password" class="form-control" name="password" id="password" value="<?php echo $edit->password; ?>" required data-valid-label="Password" minlength="6">
                </div>
                <div class="form-group">
                  <label>* Retype Password</label>
                  <input type="password" class="form-control" name="password2" id="password2" value="<?php echo $edit->password2; ?>" required data-valid-label="Retype Password" minlength="6">
                </div>
                <div class="form-group">
                  <label>* Obsolete</label>
                  <select class="form-control" name="obsolete" id="obsolete">
                    <option value="0" <?php if($edit->obsolete == "0") echo " selected"; ?>>No</option>
                    <option value="1" <?php if($edit->obsolete == "1") echo " selected"; ?>>Yes</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <button onclick="saveForm()" type="button" name="button" class="btn btn-primary"><i class="fa fa-save"></i></button>
              <a href="<?php echo set_url("user"); ?>" class="btn btn-default" title="Back"><i class="fa fa-reply"></i></a>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
function saveForm(){
  var f = $("#form1");

  if(!isValid("#username")) return;
  if(!isValid("#password")) return;
  if(!isValid("#password2")) return;

  $.ajax({
    url: '<?php echo set_url("user", "save"); ?>',
    type: 'POST',
    dataType: 'JSON',
    beforeSend: function(){
      loadingSpinner.show();
    },
    data: f.serializeArray()
  })
  .done(function(response) {
    if(response.status == true){
      alertify.alert(response.message);
      messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("user"); ?>') });
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
}
</script>

</body>
</html>
