<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>User - Admin</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>Admin</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">All Users</h3>
              <div class="box-tools pull-right">
                <a href="<?php echo set_url("admin", "user", "add"); ?>" class="btn btn-primary btn-sm" title="Add New"><i class="fa fa-file-o"></i></a>
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table class="table table-bordered table-hover" id="table1">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Obsolete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $user_level = $this->config->item("user_level");
                  foreach($data_user->result() as $row): ?>
                  <tr>
                    <td>
                      <span class="row-title"><?php echo $row->username; ?></span>
                      <?php if (get_session("auth_username") != $row->username): ?>
                      <span class="row-action">
                        <a href="<?php echo set_url("admin", "user", "edit") . "?id=" . $row->id; ?>">Edit</a> |
                        <a href="<?php echo set_url("admin", "user", "delete") . "?id=" . $row->id; ?>" class="text-danger" onclick="return confirm('Delete?');">Delete</a>
                      </span>
                      <?php endif; ?>
                    </td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $user_level[$row->level] ?></td>
                    <td class="text-center"><?php echo ($row->obsolete == 1) ? "Yes" : "No"; ?></td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
var table1;
$(function () {
  table1 = $('#table1').DataTable({
    "bDestroy": true,
    "stateSave": true,
  });
})
</script>

</body>
</html>
