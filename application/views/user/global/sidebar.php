<aside class="main-sidebar" style="padding-top:50px;">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li <?php if($this->router->fetch_class() == "dashboard") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/dashboard"); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "service") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/queue/register"); ?>">
          <i class="fa fa-pencil"></i>
          <span>Daftar</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "bpjs") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/bpjs"); ?>">
          <i class="fa fa-file-o"></i>
          <span>List BPJS</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "queue") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/queue"); ?>">
          <i class="fa fa-list"></i>
          <span>Antrian</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "setting") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/setting"); ?>">
          <i class="fa fa-gears"></i>
          <span>Pengaturan</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "logout") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("user/logout"); ?>">
          <i class="fa fa-sign-out"></i>
          <span>Logout</span>
        </a>
      </li>
    </ul>
  </section>
</aside>
