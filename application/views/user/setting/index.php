<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Setting - Home</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
  <div class="wrapper">
    <?php echo page_header(); ?>
    <?php echo page_sidebar(); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Setting
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Setting</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <?php echo show_alert(); ?>
        <div class="row">
          <div class="col-md-12">
            <h2>Ubah Informasi Akun</h2>
            <form id="fmChangePassword" method="post" action="<?php echo set_url("user", "setting", "save_account"); ?>">
              <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" readonly placeholder="" value="<?php echo get_session("auth_email"); ?>">
              </div>
              <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="" value="<?php echo get_session("auth_name"); ?>">
              </div>
              <div class="form-group">
                <label>No. Telp</label>
                <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="" value="<?php echo get_session("auth_phone_number"); ?>">
              </div>
              <button class="btn btn-primary btn-block" type="submit" form="fmChangePassword">Simpan</button>
            </form>

            <br>

            <!-- <a href="#" data-target="#modalAccount" data-toggle="modal" class="btn btn-default btn-block" style="padding-top:15px;padding-bottom:15px">Ubah Informasi Akun</a> -->
            <a href="javascript:;" data-toggle="modal" data-target="#modalChangePassword" class="btn btn-default btn-block" style="padding-top:15px;padding-bottom:15px">Ganti Password</a>
            <!-- <a href="javascript:;" class="btn btn-default btn-block" style="padding-top:15px;padding-bottom:15px">Ganti Email</a> -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <?php echo page_footer(); ?>
  </div>
  <!-- ./wrapper -->
  <?php echo assets_bottom(); ?>
  <script>
  function saveForm(fm){
    var f = $("#" + fm);

    $.ajax({
      url: '<?php echo set_url("setting", "save"); ?>',
      type: 'POST',
      dataType: 'JSON',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: f.serializeArray()
    })
    .done(function(response) {
      if(response.status == true){
        alertify.alert(response.message);
        messageBox("Alert", response.message);
      }
    })
    .fail(function(response) {
      alertify.error('Error!');
    })
    .always(function() {
      loadingSpinner.hide();
    });
  }
  </script>

</body>
</html>
