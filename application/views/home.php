<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Metas -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>E-Antrian - RSUD Balaraja</title>

    <!-- External CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/et-line.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/plyr.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>front/css/responsive.css">

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400%7CUbuntu:400,700%7COpen+Sans" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="<?php echo base_url(); ?>logo.png">

    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>front/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo base_url(); ?>front/assets/js/respond.min.js"></script>
    <![endif]-->

    <style media="screen">

    </style>
</head>

<body style="background-color:#171717;">

    <!-- Preloader -->
    <div class="loader-wrap" id="loader-wrap">
        <div class="cssload-loader"></div>
    </div>
    <!-- Preloader End -->

    <!-- Banner Area -->
    <div id="banner" class="banner">
        <div class="banner-item banner-1 steller-parallax" data-stellar-background-ratio="0.5">
            <div class="banner-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="banner-text-content">
                                <h1 class="banner-title">Aplikasi E-Antrian <br/>RSUD Balaraja</h1>
                                <div class="button-group">
                                    <a class="btn btn-lg btn-border" href="<?php echo base_url(); ?>apk/E-Antrian RSUD Balaraja_1_1.0.apk"><i class="fa fa-download"></i> Download</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 hidden-sm hidden-xs">
                            <div class="mock right-style">
                                <img class="back-mock wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="1s" src="<?php echo base_url(); ?>front/images/mocks/banner-mock-back.png" alt="mock back">
                                <img class="front-mock wow fadeInUp" data-wow-duration="1.5s" src="<?php echo base_url(); ?>front/images/mocks/banner-mock-front.png" alt="mock front">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Area End -->

    <!-- Script -->
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.nav.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/wow.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.localScroll.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/owl.carousel.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/plyr.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="<?php echo base_url(); ?>front/assets/js/jquery.stellar.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI"></script>
    <script src="<?php echo base_url(); ?>front/js/map.js"></script>
    <script src="<?php echo base_url(); ?>front/js/custom.js"></script>
</body>

</html>
