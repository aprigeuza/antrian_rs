<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Dashboard - Jadwal Libur - Add</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Jadwal Libur
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo set_url("admin/day_off"); ?>">Jadwal Libur</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <input type="hidden" name="id" value="0">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Add New</h3>

                <div class="box-tools pull-right">
                  <button onclick="saveForm()" type="button" name="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                  <a href="<?php echo set_url("admin/day_off"); ?>" class="btn btn-default btn-sm" title="Back"><i class="fa fa-reply"></i></a>
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>* Doctor</label>
                      <select class="form-control select2" name="doctor_id" required data-valid-label="Doctor">
                        <option value="0">-- Pilih Doktor --</option>
                        <?php foreach($doctor->result() as $row): ?>
                        <option value="<?php echo $row->poli_id .",". $row->id; ?>"><?php echo $row->poli_name . ' | ' . $row->name; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>* Tanggal</label>
                      <input type="text" class="form-control datepicker" id="day_off_date" placeholder="yyyy/mm/dd" name="day_off_date" value="<?php echo date("Y-m-d"); ?>" readonly style="background-color:#fff;cursor:pointer">
                    </div>
                    <div class="form-group">
                      <label>Catatan</label>
                      <textarea class="form-control" name="note" rows="8" cols="80"></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button onclick="saveForm()" type="button" name="button" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo set_url("admin/day_off"); ?>" class="btn btn-default" title="Back"><i class="fa fa-reply"></i></a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
$(document).ready(function() {
  $(".select2").select2();
  $(".datepicker").datepicker({
    format: 'yyyy/mm/dd',
    autoclose: true,
    orientation: 'bottom'
  });
});
function saveForm(){
  var f = $("#form1");
  if(!isValid("#doctor_id")) return;
  if(!isValid("#day_off_date")) return;

  $.ajax({
    url: '<?php echo set_url("admin/day_off", "save"); ?>',
    type: 'POST',
    dataType: 'JSON',
    beforeSend: function(){
      loadingSpinner.show();
    },
    data: f.serializeArray()
  })
  .done(function(response) {
    if(response.status == true){
      alertify.alert(response.message);
      messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("admin/day_off"); ?>') });
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
}
</script>

</body>
</html>
