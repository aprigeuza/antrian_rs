<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Admin - Poli - Edit</title>
  <?php echo assets_top(); ?>
</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Poli
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo set_url("admin/poli"); ?>">Poli</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <form id="form1">
        <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Edit New</h3>

                <div class="box-tools pull-right">
                  <button onclick="saveForm()" type="button" name="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                  <a href="<?php echo set_url("admin/poli"); ?>" class="btn btn-default btn-sm" title="Back"><i class="fa fa-reply"></i></a>
                  <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>* Nama</label>
                      <input type="text" class="form-control" name="name" id="name" value="<?php echo $edit->name; ?>" required data-valid-label="Nama" minlength="2">
                    </div>
                    <div class="form-group">
                      <label>* Kode</label>
                      <input type="text" class="form-control" name="prefix_code" id="prefix_code" value="<?php echo $edit->prefix_code; ?>" required data-valid-label="Prefix" minlength="1" readonly>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select class="form-control" name="status">
                        <option value="active"  <?php if($edit->status=='active') echo ' selected'; ?>>Aktif</option>
                        <option value="inactive"  <?php if($edit->status=='inactive') echo ' selected'; ?>>InActive</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Maksimal Pengunjung</label>
                      <input type="text" class="form-control numberFormat text-right" name="max_visitor" id="max_visitor" value="<?php echo $edit->max_visitor; ?>" data-precision="0">
                    </div>
                    <div class="form-group">
                      <label>Jam Tutup</label>
                      <input type="text" class="form-control timepicker" name="close_time" id="close_time" value="<?php echo $edit->close_time; ?>" data-precision="0" readonly style="background-color:#fff;cursor:pointer">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button onclick="saveForm()" type="button" name="button" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo set_url("admin/poli"); ?>" class="btn btn-default" title="Back"><i class="fa fa-reply"></i></a>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
function saveForm(){
  var f = $("#form1");

  if(!isValid("#name")) return;
  if(!isValid("#prefix_code")) return;

  $.ajax({
    url: '<?php echo set_url("admin/poli", "save"); ?>',
    type: 'POST',
    dataType: 'JSON',
    beforeSend: function(){
      loadingSpinner.show();
    },
    data: f.serializeArray()
  })
  .done(function(response) {
    if(response.status == true){
      alertify.alert(response.message);
      messageBox("Alert", response.message, function(){ redirect('<?php echo set_url("admin/poli"); ?>') });
    }
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
}
</script>

</body>
</html>
