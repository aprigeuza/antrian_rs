<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Antrian - Admin</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Antrian
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Antrian</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Data Antrian</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <form id="form1">
                <div class="row">
                  <div class="col-md-1" style="width:auto">
                    <label style="line-height:35px;">Tanggal</label>
                  </div>
                  <div class="col-md-2">
                    <input type="text" class="form-control datepicker" name="visit_date" value="<?php echo $this->session->userdata("ses_queue_list_filter_date"); ?>">
                  </div>
                  <div class="col-md-2">
                    <div class="visible-xs">
                      <br>
                    </div>
                    <button type="button" name="button" class="btn btn-primary" onclick="loadTable();">Filter</button>
                  </div>
                </div>
              </form>

              <div style="width:100%;overflow:auto;padding-bottom:15px;" id="table-placeholder">

              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script>
  function loadTable(){
    var f = $("#form1");
    $.ajax({
      url: '<?php echo set_url("admin/queue_list/get_table"); ?>',
      type: 'POST',
      dataType: 'HTML',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: f.serializeArray()
    })
    .done(function(response) {
      $("#table-placeholder").html(response);
    })
    .fail(function(response) {
      alertify.error('Error!');
    })
    .always(function() {
      loadingSpinner.hide();
    });
  }

  $(document).ready(function() {
    $(".datepicker").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true
    });


    loadTable();
  });
</script>


</body>
</html>
