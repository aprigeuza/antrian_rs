<table class="table table-bordered table-hover" id="table1" border="1">
  <thead>
    <tr>
      <th>Layanan</th>
      <th>Tanggal Daftar</th>
      <th>Tanggal Kunjungan</th>
      <th>Kode</th>
      <th>Nomor Antrian</th>
      <th>Pendaftar</th>
      <th>Email</th>
      <th>No. Telp</th>
      <th>BPJS#</th>
      <th>Pasien</th>
      <th>Dokter</th>
      <th>No. Telp</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($list->result() as $row): ?>
      <tr>
        <td><?php echo $row->poli_name; ?></td>
        <td><?php echo $row->register_date; ?></td>
        <td><?php echo $row->visit_date; ?></td>
        <td><?php echo $row->queue_code; ?></td>
        <td><?php echo $row->queue_number; ?></td>
        <td><?php echo $row->user_name; ?></td>
        <td><?php echo $row->user_email; ?></td>
        <td><?php echo $row->user_phone_number; ?></td>
        <td><?php echo $row->bpjs_number; ?></td>
        <td><?php echo $row->bpjs_name; ?></td>
        <td><?php echo $row->doctor_name; ?></td>
        <td><?php echo $row->doctor_phone_number; ?></td>
        <td class="text-center"><?php echo $row->status; ?></td>
      </tr>
    <?php endforeach ?>
  </tbody>
</table>
