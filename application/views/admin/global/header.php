

<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>R</b>B</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>RSUD</b> Balaraja</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li>
          <a href="<?php echo base_url(); ?>" target="_blank" title="View">
            <i class="fa fa-share"></i>
          </a>
        </li>
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user-o"></i>
            <span class="hidden-xs">Welcome, <?php echo get_session("auth_username"); ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <i class="fa fa-user fa-3x" style="color:#fff"></i>
              <p>
                <?php echo get_session("auth_name"); ?>
                <small>Last Loged-in : <br><?php echo get_session("auth_last_login"); ?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="javascript:;" data-toggle="modal" data-target="#modalChangePassword" class="btn btn-primary btn-flat">Change Password</a>
              </div>
              <div class="pull-right">
                <a href="<?php echo set_url("admin/logout"); ?>" class="btn btn-warning btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>


<form action="<?php echo set_url("admin", "user", "change_password"); ?>" method="post">
  <div class="modal fade" id="modalChangePassword" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="">Change Password</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>* Old Password</label>
            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password" required>
          </div>
          <hr>
          <div class="form-group">
            <label>* New Password</label>
            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" required>
          </div>
          <div class="form-group">
            <label>* New Password Again</label>
            <input type="password" class="form-control" id="new_password2" name="new_password2" placeholder="New Password" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </div>
  </div>
</form>
