<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <?php if(in_array(get_session("auth_level"), ["sa", "user"])): ?>
      <li <?php if($this->router->fetch_class() == "dashboard") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/dashboard"); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "poli") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/poli"); ?>">
          <i class="fa fa-pencil"></i>
          <span>Poli</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "doctor") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/doctor"); ?>">
          <i class="fa fa-cube"></i>
          <span>Doktor</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "queue_list") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/queue_list"); ?>">
          <i class="fa fa-list"></i>
          <span>Antrian</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "day_off") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/day_off"); ?>">
          <i class="fa fa-calendar"></i>
          <span>Jadwal Libur</span>
        </a>
      </li>
      <li <?php if($this->router->fetch_class() == "report") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/report"); ?>">
          <i class="fa fa-pie-chart"></i>
          <span>Laporan</span>
        </a>
      </li>
      <?php endif; ?>
      <?php if(in_array(get_session("auth_level"), ["doc_checking"])): ?>
        <li <?php if($this->router->fetch_class() == "dashboard") echo 'class="active"'; ?>>
          <a href="<?php echo set_url("admin/dashboard"); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li <?php if($this->router->fetch_class() == "doc_checking") echo 'class="active"'; ?>>
          <a href="<?php echo set_url("admin/doc_checking"); ?>">
            <i class="fa fa-file"></i>
            <span>Document Checking</span>
          </a>
        </li>
      <?php endif; ?>
      <?php if (get_session("auth_level") == "sa"): ?>
      <li <?php if($this->router->fetch_class() == "user") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/user"); ?>">
          <i class="fa fa-users"></i>
          <span>User</span>
        </a>
      </li>
      <?php endif; ?>
      <li <?php if($this->router->fetch_class() == "logout") echo 'class="active"'; ?>>
        <a href="<?php echo set_url("admin/logout"); ?>">
          <i class="fa fa-sign-out"></i>
          <span>Logout</span>
        </a>
      </li>
    </ul>
  </section>
</aside>
