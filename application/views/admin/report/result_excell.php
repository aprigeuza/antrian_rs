<table border="1">
  <thead>
    <tr>
      <th>#</th>
      <th>Poli</th>
      <th>Tanggal Kunjungan</th>
      <th>Kode</th>
      <th>Nomor Antrian</th>
      <th>Pendaftar</th>
      <th>Email</th>
      <th>Nomor Telp</th>
      <th>BPJS#</th>
      <th>Pasien</th>
      <th>Dokter</th>
      <th>Tanggal Daftar</th>
      <th>Status</th>
      <th>Catatan</th>
      <th>Operator</th>
      <th>Pemeriksa Dokumen</th>
    </tr>
  </thead>
  <tbody><?php
    $n = 1;
    foreach($result_data->result() as $row):
      echo "\n"
      ?>
    <tr>
      <td><?php echo $n++; ?></td>
      <td><?php echo $row->poli_name; ?></td>
      <td><?php echo $row->visit_date; ?></td>
      <td><?php echo $row->queue_code; ?></td>
      <td><?php echo $row->queue_number; ?></td>
      <td><?php echo $row->user_name; ?></td>
      <td><?php echo $row->user_email; ?></td>
      <td><?php echo $row->user_phone_number; ?></td>
      <td><?php echo $row->bpjs_number; ?></td>
      <td><?php echo $row->bpjs_name; ?></td>
      <td><?php echo $row->doctor_name; ?></td>
      <td><?php echo $row->register_date; ?></td>
      <td><?php echo ucfirst($row->status); ?></td>
      <td><?php echo $row->note; ?></td>
      <td><?php echo $row->operator_name; ?></td>
      <td><?php echo $row->verificator_name; ?></td>
    </tr><?php endforeach; ?>
  </tbody>
</table>
