<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Result</h3>
  </div>
  <div class="box-body">
    <p class="text-info" style="font-style:italic">Menampilkan hasil dari <b><?php echo $date_start; ?></b> sampai <b><?php echo $date_end; ?></b>, status : <b><?php echo ucfirst($status); ?></b> </p>
    <table class="table table-bordered" id="tableResult">
      <thead>
        <tr>
          <th>#</th>
          <th>Poli</th>
          <th>Tanggal Kunjungan</th>
          <th>Kode</th>
          <th>Nomor Antrian</th>
          <th>Pendaftar</th>
          <th>Email</th>
          <th>Nomor Telp</th>
          <th>BPJS#</th>
          <th>Pasien</th>
          <th>Dokter</th>
          <th>Tanggal Daftar</th>
          <th>Status</th>
          <th>Catatan</th>
          <th>Operator</th>
          <th>Pemeriksa Dokumen</th>
        </tr>
      </thead>
      <tbody><?php
        $n = 1;
        foreach($result_data->result() as $row):
          echo "\n"
          ?>
        <tr>
          <td><?php echo $n++; ?></td>
          <td><?php echo $row->poli_name; ?></td>
          <td><?php echo $row->visit_date; ?></td>
          <td><?php echo $row->queue_code; ?></td>
          <td><?php echo $row->queue_number; ?></td>
          <td><?php echo $row->user_name; ?></td>
          <td><?php echo $row->user_email; ?></td>
          <td><?php echo $row->user_phone_number; ?></td>
          <td><?php echo $row->bpjs_number; ?></td>
          <td><?php echo $row->bpjs_name; ?></td>
          <td><?php echo $row->doctor_name; ?></td>
          <td><?php echo $row->register_date; ?></td>
          <td><?php echo ucfirst($row->status); ?></td>
          <td><?php echo $row->note; ?></td>
          <td><?php echo $row->operator_name; ?></td>
          <td><?php echo $row->verificator_name; ?></td>
        </tr><?php endforeach; ?>
      </tbody>
    </table>

    <a href="<?php echo set_url("admin", "report", "download_excell"); ?>" class="btn btn-default"> <i class="fa fa-download"></i> Download Excell</a>
  </div>
</div>


<!-- LINE CHART -->
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Menampilkan Chart untuk tahun <b><?php echo $y; ?></b> </h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>
  <div class="box-body">
    <div class="chart">
      <canvas id="lineChart" style="height:250px"></canvas>
    </div>

    <p>* Keterangan <br><span class="text-success">Finish : Garis Hijau </span> <br> <span class="text-danger">Cancel : Garis Merah</span> </p>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->


<script>
  $(document).ready(function() {


    $("#tableResult").DataTable({
      "bDestroy": true,
      "stateSave": true,
      "drawCallback":function(settings){
        var d = $(".dataTables_wrapper").find("div.row").eq(1).find("div").eq(0);
        d.addClass('table-responsive');
        d.css("width", (d.width()) + "px");
        d.css("margin-right", (0) + "px");
        d.css("margin-left", (15) + "px");
        d.css("padding-left", (0) + "px");
      }
    });
  });
</script>

<script>
  $(function(){

    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label               : 'Finish',
          strokeColor         : 'green',
          fillColor           : 'green',
          data                : <?php echo json_encode($chart_data_finish) . "\n"; ?>
        },
        {
          label               : 'Cancel',
          strokeColor         : 'red',
          fillColor           : 'red',
          data                : <?php echo json_encode($chart_data_cancel) . "\n"; ?>
        }
      ]
    }

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale               : true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines      : false,
      //String - Colour of the grid lines
      scaleGridLineColor      : 'rgba(0,0,0,.05)',
      //Number - Width of the grid lines
      scaleGridLineWidth      : 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines  : true,
      //Boolean - Whether the line is curved between points
      bezierCurve             : true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension      : 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot                : false,
      //Number - Radius of each point dot in pixels
      pointDotRadius          : 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth     : 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius : 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke           : true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth      : 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill             : true,
      //String - A legend template
      legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio     : true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive              : true
    }

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
    var lineChart                = new Chart(lineChartCanvas)
    var lineChartOptions         = areaChartOptions
    lineChartOptions.datasetFill = false
    lineChart.Line(areaChartData, lineChartOptions)


  })
</script>
