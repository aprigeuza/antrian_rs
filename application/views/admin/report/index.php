<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Report - Admin</title>
  <?php echo assets_top(); ?>

</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-3">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Report</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form id="form-filter">
                <h3>Filter</h3>
                <div class="form-group">
                  <label>Dari Tanggal</label>
                  <input type="text" class="form-control datepicker" id="date_start" name="date_start" placeholder="" value="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="form-group">
                  <label>Sampai Tanggal</label>
                  <input type="text" class="form-control datepicker" id="date_end" name="date_end" placeholder="" value="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="form-group">
                  <label>Status</label>
                  <select class="form-control" name="status">
                    <option value="all">--Semua--</option>
                    <option value="new">Baru</option>
                    <option value="cancel">Batal</option>
                    <option value="finish">Selesai</option>
                  </select>
                </div>
                <button type="button" id="btnFilter" class="btn btn-primary">Filter</button>
              </form>
            </div>
          </div>
        </div>

        <div class="col-md-9">
          <div id="result_placeholder">
            <p class="text-info">Silahkan filter data untuk menampilkan hasil.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>

<script src="<?php echo base_url(); ?>bower_components/chart.js/Chart.js"></script>

<script>
  $(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });


  $("#btnFilter").on("click", function(){
    var f = $("#form-filter");

    $.ajax({
      url: '<?php echo set_url("admin", "report", "get_result"); ?>',
      type: 'POST',
      dataType: 'HTML',
      beforeSend: function(){
        loadingSpinner.show();
      },
      data: f.serializeArray()
    })
    .done(function(response) {
      $("#result_placeholder").html(response);
    })
    .fail(function(response) {
      alertify.error('Error!');
    })
    .always(function() {
      loadingSpinner.hide();
    });
  });
</script>
</body>
</html>
