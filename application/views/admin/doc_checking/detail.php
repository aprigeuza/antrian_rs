<?php
$bpjs_img = base_url("img/no-img.png");
if ($detail->bpjs_img)
{
  $bpjs_img = base_url("files/" . $detail->bpjs_img);
}
$ktp_img = base_url("img/no-img.png");
if ($detail->ktp_img)
{
  $ktp_img = base_url("files/" . $detail->ktp_img);
}
$kk_img = base_url("img/no-img.png");
if ($detail->kk_img)
{
  $kk_img = base_url("files/" . $detail->kk_img);
}
$sr_img = base_url("img/no-img.png");
if ($detail->sr_img)
{
  $sr_img = base_url("files/" . $detail->sr_img);
}
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title" id="myModalLabel">Detail</h4>
</div>
<div class="modal-body form-horizontal">
<div class="form-horizontal">
  <div class="form-group">
    <label class="control-label col-md-4 text-left">Nomor BPJS</label>
    <div class="col-md-8">
      <div class="form-control"><?php echo $detail->bpjs_number; ?></div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-4 text-left">Nama</label>
    <div class="col-md-8">
      <div class="form-control"><?php echo $detail->name; ?></div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-4 text-left">Tanggal Lahir</label>
    <div class="col-md-8">
      <div class="form-control"><?php echo $detail->birthdate; ?></div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-3">
      <a class="thumbnail fancybox" onclick="" href="<?php echo $bpjs_img; ?>">
        <div class="iimg" style="background-image:url('<?php echo $bpjs_img; ?>');"></div>
        <p class="text-center">Kartu BPJS</p>
      </a>
    </div>
    <div class="col-md-3">
      <a class="thumbnail fancybox" onclick="" href="<?php echo $ktp_img; ?>">
        <div class="iimg" style="background-image:url('<?php echo $ktp_img; ?>');"></div>
        <p class="text-center">Kartu Tanda Penduduk</p>
      </a>
    </div>
    <div class="col-md-3">
      <a class="thumbnail fancybox" onclick="" href="<?php echo $kk_img; ?>">
        <div class="iimg" style="background-image:url('<?php echo $kk_img; ?>');"></div>
        <p class="text-center">Kartu Keluarga</p>
      </a>
    </div>
    <div class="col-md-3">
      <a class="thumbnail fancybox" onclick="" href="<?php echo $sr_img; ?>">
        <div class="iimg" style="background-image:url('<?php echo $sr_img; ?>');"></div>
        <p class="text-center">Surat Rujukan <br>Berlaku Sampai : <br><?php echo date("d M Y", strtotime($detail->sr_valid_date)); ?></p>
      </a>
    </div>
  </div>
</div>
  <form class="" action="<?php echo set_url("admin", "doc_checking","reject") ?>" method="get" id="fmVerification">
    <input type="hidden" name="id" value="<?php echo $detail->id; ?>">
    <div class="form-group" style="margin-left:0px; margin-right:0px">
      <label for="">Tambahkan Info</label>
      <input type="text" class="form-control" name="verification_info" id="verification_info" value="<?php echo $detail->verification_info; ?>">
    </div>
  </form>
</div>
<div class="modal-footer">
  <div class="pull-left">
    <a href="<?php echo set_url("admin", "doc_checking", "verification") . "?id=" . $detail->id; ?>" onclick="return confirm('Verifikasi?');" class="btn btn-primary">Verifikasi</a>
    <button type="button" class="btn btn-danger" id="btnReject">Reject</button>
    <!-- <a href="<?php echo set_url("admin", "doc_checking", "reject") . "?id=" . $detail->id; ?>" onclick="return confirm('Reject?')" class="btn btn-danger">Tolak</a> -->
  </div>
  <div class="pull-right">
    <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn btn-default">Tutup</a>
  </div>
</div>


<script>
  $(".fancybox").fancybox({
		'transitionIn'	:	'elastic',
		'transitionOut'	:	'elastic',
		'speedIn'		:	600,
		'speedOut'		:	200,
		'overlayShow'	:	false
	});
</script>

<script>
  $("#btnReject").on("click", function(e){
    if(confirm('Apakah Anda yakin akan me-Reject data?'))
    {
      $("#fmVerification").submit();
    }
  })
</script>
