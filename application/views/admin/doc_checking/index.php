<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Document Checking - Admin</title>
  <?php echo assets_top(); ?>

  <style>
    .iimg{
      width: 100%;
      height: 100px;
      background-position: center;
      background-repeat: no-repeat;
      background-size: contain;
      border: 1px solid rgb(153, 176, 181);
      border-radius: 3px;
    }
  </style>

</head>
<body class="hold-transition <?php echo $this->config->item("admin_theme"); ?> sidebar-mini">
<div class="wrapper">
  <?php echo page_header(); ?>
  <?php echo page_sidebar(); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Document Checking
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Document Checking</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo show_alert(); ?>
      <div class="row">
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Outstanding Document Checking</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table class="table table-bordered table-hover" id="table1">
                <thead>
                  <tr>
                    <th>No BPJS</th>
                    <th>Nama BPJS</th>
                    <th>Tanggal Lahir</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($data_outstanding->result() as $row): ?>
                  <tr onclick="viewDetail('<?php echo $row->id; ?>')" style="cursor:pointer">
                    <td><?php echo $row->bpjs_number; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->birthdate; ?></td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">All</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" title="minimaze/maximaze"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table class="table table-bordered table-hover" id="table2">
                <thead>
                  <tr>
                    <th>No BPJS</th>
                    <th>Nama BPJS</th>
                    <th>Verifikasi Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($data_all->result() as $row): ?>
                  <tr onclick="viewDetail('<?php echo $row->id; ?>')" style="cursor:pointer">
                    <td><?php echo $row->bpjs_number; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->verification_status; ?></td>
                  </tr>
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php echo page_footer(); ?>

  <?php echo page_sidebar_control(); ?>
</div>
<!-- ./wrapper -->
<?php echo assets_bottom(); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" charset="utf-8"></script>
<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Launch demo modal</button>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modalContent">
    </div>
  </div>
</div>

<script>
var table1;
var table2;
$(function () {
  table1 = $('#table1').DataTable({
    "bDestroy": true,
    "stateSave": true,
  });

  table2 = $('#table2').DataTable({
    "bDestroy": true,
    "stateSave": true,
  });

});


function viewDetail(id){
  $.ajax({
    url: '<?php echo set_url("admin/doc_checking/detail"); ?>',
    type: 'POST',
    dataType: 'HTML',
    beforeSend: function(){
      loadingSpinner.show();
    },
    data: {
      id : id
    }
  })
  .done(function(response) {
    $("#modalContent").html(response);
    $("#myModal").modal("show");
  })
  .fail(function(response) {
    alertify.error('Error!');
  })
  .always(function() {
    loadingSpinner.hide();
  });
}



</script>

</body>
</html>
